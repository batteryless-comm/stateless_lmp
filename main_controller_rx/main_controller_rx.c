/*
 * Main application controller that communicates with a radio board and a persistent clock
 */
//#include <msp430.h>
#include <stdio.h>
#include <string.h>
/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Clock.h>

/* TI-RTOS Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/SPI.h>

/* Board Header file */
#include "Board.h"

#define DEBUG                   (0)

#define TASKSTACKSIZE           (768)
#define RX_DATA_LENGTH          (2)

#define MAX_LOOP                (10)
#define SPI_MSG_LENGTH          (5)
#define FASTDIE_CONFIG_LENGTH   (5)
#define BOOT_DELAY              (0)
#define NUM_PKTS_WAIT           (1) // number of packets to wait for reception after a successful on period


#pragma PERSISTENT(payloadRx)
uint8_t payloadRx[SPI_MSG_LENGTH] = {0};


/* Create structs for tasks/threads */
Task_Struct getTimeTask;
static Task_Params getTimeTaskParams;
static  Char getTimeTaskStack[TASKSTACKSIZE];

Task_Struct radioTask;
static Task_Params radioTaskParams;
static  Char radioTaskStack[TASKSTACKSIZE];

UART_Handle uart;
UART_Params uartParams;

/* Create get time Semaphore for time info */
static Semaphore_Handle getTimeSem;
/* Semaphore to block master until peripheral is ready for transfer */
static Semaphore_Handle controllerSem;
//static Semaphore_Handle bootDelaySem;


/* SPI rx and tx buffers */
uint8_t controllerRxBuffer[SPI_MSG_LENGTH];
uint8_t controllerTxBuffer[SPI_MSG_LENGTH];

/*
 * 0 = No fast die - NULL LMP
 * 1 = Fast die after 1 no ack
 * .
 * .
 * n = Fast die after 'n' no acks
 */
#pragma PERSISTENT(fastDieConfigs)
uint8_t fastDieConfigs[FASTDIE_CONFIG_LENGTH] = {1, 5, 10, 15, 0};
//uint8_t fastDieConfigs[fastDieConfigLength] = {10, 1, 15};

#pragma PERSISTENT(fastDieSelect)
uint8_t fastDieSelect = 0;
#pragma PERSISTENT(fastDieConfig)
uint8_t fastDieConfig = 1;
uint8_t noRxCount = 0;
#pragma PERSISTENT(updateFastDieConfig)
bool updateFastDieConfig = false;
#pragma PERSISTENT(lfCount)
uint16_t lfCount = 0;

//bool updateFastDieConfig = false;
//bool succRxFlag = false;

char uartStr1[20];
char uartStr2[10];

/*
 *  Callback function for the GPIO interrupt on P5.3.
 */
void getTimeGPIOFxn(unsigned int index) {
    Semaphore_post(getTimeSem);
}

void radioReadyFxn(unsigned int index) {
    Semaphore_post(controllerSem);
}

/* Callback function for the GPIO interrupt on P4.3. */
void setFastDieConfigFxn(unsigned int index) {
    updateFastDieConfig = true;
}

/* Callback function for the GPIO interrupt on Board_BUTTON0 S1. */
void gpioButtonFxn(unsigned int index) {
//    fastDieConfig = 1; //reset fast-die config
//    fastDieSelect = 0;
//    fastDieConfig = fastDieConfigs[fastDieSelect];
    noRxCount = 0;
}

void radioFxn(UArg arg0, UArg arg1) {
    SPI_Handle      controllerSpi;
    SPI_Params      spiParams;
    SPI_Transaction transaction;
    bool            transferOK;
    uint16_t        seqNumber;
    uint8_t         i;
    uint8_t         rxTimeOut = 5; //ms
    bool            sendFastDieConfig;
    uint8_t         timeoutLMP = 1;
    uint8_t         toutSuccRx = 1;

#if DEBUG
    char            uartStr[100];
#endif

//    Semaphore_pend(bootDelaySem, (BOOT_DELAY / Clock_tickPeriod));
//    GPIO_write(POWER_MONITOR, 1);
    // Turn on the radio board
//    GPIO_write(POWER_RADIO, 1);

    /* install Button callback */
    GPIO_setCallback(Board_BUTTON0, gpioButtonFxn);
    /* Enable interrupts */
    GPIO_enableInt(Board_BUTTON0);

//    // Enable interrupt on Pin 4.3
//    GPIO_setConfig(CONFIG_FAST_DIE, GPIO_CFG_INPUT | GPIO_CFG_IN_PD | GPIO_CFG_IN_INT_RISING);
//    /* install GPIO callback */
//    GPIO_setCallback(CONFIG_FAST_DIE, setFastDieConfigFxn);
//    /* Enable interrupts */
//    GPIO_enableInt(CONFIG_FAST_DIE);

    /* Configure spi controller and peripheral ready gpio pins */
    GPIO_setConfig(CONFIG_SPI_CONTROLLER_READY, GPIO_CFG_OUTPUT | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_SPI_RADIO_READY, GPIO_CFG_INPUT);

    /* Set CONTROLLER ready pin */
    GPIO_write(CONFIG_SPI_CONTROLLER_READY, 1);
    UART_write(uart, "CONFIG_SPI_CONTROLLER_READY\n", sizeof("CONFIG_SPI_CONTROLLER_READY\n"));

    /* Wait for peripheral to be ready */
    while (GPIO_read(CONFIG_SPI_RADIO_READY) == 0) {}
#if DEBUG
    UART_write(uart, "handshake complete! \n", sizeof("handshake complete! \n"));
#endif

    /* Handshake complete; now configure interrupt on CONFIG_SPI_SLAVE_READY */
    GPIO_setConfig(CONFIG_SPI_RADIO_READY, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);
    GPIO_setCallback(CONFIG_SPI_RADIO_READY, radioReadyFxn);
    GPIO_enableInt(CONFIG_SPI_RADIO_READY);
#if DEBUG
    UART_write(uart, "configure interrupt on RADIO\n", sizeof("configure interrupt on RADIO\n"));
#endif
    /* Create a semaphore; the CONTROLLER will wait on this semaphore
     * until the peripheral is ready. */
    Semaphore_Params params;
    Error_Block eb;
    /* Init params */
    Semaphore_Params_init(&params);
    Error_init(&eb);
    /* Create semaphore instance */
    controllerSem = Semaphore_create(0, &params, &eb);
#if DEBUG
    if(controllerSem == NULL) {
        System_printf("Semaphore creation failed");
        System_flush();
        UART_write(uart, "Semaphore creation failed\n", sizeof("Semaphore creation failed\n"));
    }
#endif

    /* Open SPI as CONTROLLER */
    SPI_Params_init(&spiParams);
    spiParams.frameFormat = SPI_POL0_PHA1;
    spiParams.mode = SPI_MASTER;
    spiParams.bitRate = 1000000;
    controllerSpi = SPI_open(Board_SPI0, &spiParams);

#if DEBUG
    if (controllerSpi == NULL) {
        UART_write(uart, "Error initializing CONTROLLER SPI\n", sizeof("Error initializing CONTROLLER SPI\n"));
         while (1);
     } else {
         UART_write(uart, "CONTROLLER SPI initialized\n", sizeof("CONTROLLER SPI initialized\n"));
     }
#endif
    /*
     * CONTROLLER has opened CONFIG_SPI_CONTROLLER; set CONFIG_SPI_CONTROLLER_READY low to
     * inform the peripheral.
     */
    GPIO_write(CONFIG_SPI_CONTROLLER_READY, 0);

    /* Initialize master SPI transaction structure */
    memset((void *) controllerRxBuffer, 0, SPI_MSG_LENGTH);
    transaction.count = SPI_MSG_LENGTH;
    transaction.txBuf = (void *) controllerTxBuffer;
    transaction.rxBuf = (void *) controllerRxBuffer;

    // send the fast-die config to radio board for first time after boot-up
    sendFastDieConfig = true;
//    timeoutLMP = fastDieConfig*rxTimeOut;

    while(1) {
        // If there is a fast-die config change from sniffer node and it's the first lifecycle
//        if(updateFastDieConfig && lfCount == 1) {
        if(updateFastDieConfig) {
            fastDieSelect++; // select next fast-die config
            if(fastDieSelect < FASTDIE_CONFIG_LENGTH){
                fastDieConfig = fastDieConfigs[fastDieSelect];
            } else {
                fastDieSelect = 0;  // resets the config
                fastDieConfig = fastDieConfigs[fastDieSelect];
            }
            sendFastDieConfig = true;       // send new config to radio
            GPIO_write(SEND_LMP_CONFIG, 1);
            updateFastDieConfig = false;
            GPIO_write(UPDATE_FD_CONFIG_OK, 1);

//            timeoutLMP = fastDieConfig*rxTimeOut;

#if DEBUG
            sprintf(uartStr1, "Fast-die Config = %d\n", fastDieConfig);
            UART_write(uart, uartStr1, sizeof(uartStr1));
            sprintf(uartStr1, "Time-out = %d\n", timeoutLMP);
            UART_write(uart, uartStr1, sizeof(uartStr1));
#endif

        }

        if(sendFastDieConfig){
            timeoutLMP = fastDieConfig*rxTimeOut;
            toutSuccRx = NUM_PKTS_WAIT*rxTimeOut;
            // Clear the transferOK flag */
            transferOK = false;

            /* Copy message to transmit buffer */
            controllerTxBuffer[0] = timeoutLMP;
            controllerTxBuffer[1] = toutSuccRx;
            controllerTxBuffer[2] = 0x0;
            controllerTxBuffer[3] = 0x0;
            /*
             * Wait until peripheral is ready for transfer; slave will pull
             * CONFIG_SPI_PERIPHERAL_READY low.
             */
            Semaphore_pend(controllerSem, BIOS_WAIT_FOREVER);

            // Send the time out to the radio board
            // pull the peripheral select line low
            GPIO_write(PERIPHERAL_SELECT, 0);
            /* Perform SPI transfer */
            transferOK = SPI_transfer(controllerSpi, &transaction);
            GPIO_write(PERIPHERAL_SELECT, 1);

            sendFastDieConfig = false;
        }

#if DEBUG
    if (transferOK) {
        UART_write(uart, "time out sent\n", sizeof("time out sent\n"));
    } else {
        UART_write(uart, "time out sent failed\n", sizeof("time out sent failed\n"));
    }
#endif


        /* Copy message to transmit buffer */
        controllerTxBuffer[0] = (uint8_t)(seqNumber >> 8);
        controllerTxBuffer[1] = (uint8_t)(seqNumber++);
        controllerTxBuffer[2] = 0x30;
        controllerTxBuffer[3] = 0x40;

        // Clear the transferOK flag */
        transferOK = false;
        /*
         * Wait until peripheral is ready for transfer; slave will pull
         * CONFIG_SPI_PERIPHERAL_READY low.
         */
        Semaphore_pend(controllerSem, BIOS_WAIT_FOREVER);

#if DEBUG
        /* Toggle user LED, indicating a SPI transfer is in progress */
        GPIO_toggle(Board_LED1);
        UART_write(uart, "Transfer in progress\n", sizeof("Transfer in progress\n"));
#endif

        // pull the peripheral select line low
        GPIO_write(PERIPHERAL_SELECT, 0);
        /* Perform SPI transfer */
        transferOK = SPI_transfer(controllerSpi, &transaction);
        GPIO_write(PERIPHERAL_SELECT, 1);

#if DEBUG
        sprintf(uartStr, "%d", transferOK );
        UART_write(uart, "transfer ok = ", sizeof("transfer ok = "));
        UART_write(uart, uartStr, sizeof(uartStr));
        UART_write(uart, "\n", sizeof("\n"));
#endif

        if (transferOK) {
            /* Check if we received the data from the sender */
            if(controllerRxBuffer[SPI_MSG_LENGTH-1]) {
                // Save the packet received to NVM - FRAM
                for(i=0; i<SPI_MSG_LENGTH; i++){
                    payloadRx[i] = controllerRxBuffer[i];
                }
//                succRxFlag == true;
                // Do not fast die
                GPIO_write(FAST_DIE, 0);
            } else {
                noRxCount++;  // increment no rx count
                // update successful rx flag
//                if (succRxFlag == true && fastDieConfig != 0){
////                    succRxFlag = true;
//                    fastDieConfig = NUM_PKTS_WAIT;
//                    sendFastDieConfig = true;       // send new config to radio
//                    GPIO_write(SEND_LMP_CONFIG, 1); // Generate interrupt at radio to receive config
//                }
                // Set Fast die GPIO depending on current fast-die settings
                if(fastDieConfig != 0){
                    GPIO_write(FAST_DIE, 1);
                    Task_exit();
//                    UART_write(uart, "Fast die\n", sizeof("Fast die\n"));
                } else {
                    GPIO_write(FAST_DIE, 0);
                }
            }

#if DEBUG
           sprintf(uartStr, "M = %x\n", controllerRxBuffer[4]);
           UART_write(uart, uartStr, sizeof(uartStr));
           UART_write(uart, "\n", sizeof("\n"));
#endif
        } else {
#if DEBUG
            UART_write(uart, "\nUnsuccessful Master received\n", sizeof("Unsuccessful Master received\n"));
#endif
        }
    }
}

Void getTimeFxn(UArg arg0, UArg arg1)
{
    uint8_t         j=0;
    uint8_t         txBuffer[1];
    uint8_t         rxBuffer[RX_DATA_LENGTH];
    I2C_Handle      i2c;
    I2C_Params      i2cParams;
    I2C_Transaction i2cTransaction;

    /* install Button callback */
    GPIO_setCallback(Board_BUTTON0, getTimeGPIOFxn);
    /* Enable interrupts */
    GPIO_enableInt(Board_BUTTON0);

    UART_write(uart, "GPIO Callback set\n", sizeof("GPIO Callback set\n"));

    // Enable interrupt on Pin 5.3
//    GPIO_setConfig(GET_TIME_GPIO, GPIO_CFG_INPUT | GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);
//    /* install Button callback */
//    GPIO_setCallback(GET_TIME_GPIO, getTimeGPIOFxn);
//    /* Enable interrupts */
//    GPIO_enableInt(GET_TIME_GPIO);


    /* Create a semaphore */
    Semaphore_Params params;
    Error_Block eb;

    /* Init params */
    Semaphore_Params_init(&params);
    Error_init(&eb);
    /* Create semaphore instance */
    getTimeSem = Semaphore_create(0, &params, &eb);
    if(getTimeSem == NULL) {
        System_abort("Semaphore creation failed");
    }

    UART_write(uart, "Semaphore created\n", sizeof("Semaphore created\n"));


    /* Create I2C for usage */
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_400kHz;
    i2cParams.transferMode = I2C_MODE_BLOCKING;

    i2c = I2C_open(Board_I2C0, &i2cParams);
    if (i2c == NULL) {
        System_abort("Error Initializing I2C\n");
    }
    else {
        System_printf("I2C Initialized!\n");
    }
    UART_write(uart, "I2C initialized\n", sizeof("I2C initialized\n"));

    /* Point to the T ambient register and read its 2 bytes */
    txBuffer[0] = 0x01;
    i2cTransaction.slaveAddress = 0x0048;
    i2cTransaction.writeBuf = txBuffer;
    i2cTransaction.writeCount = 0;
    i2cTransaction.readBuf = rxBuffer;
    i2cTransaction.readCount = RX_DATA_LENGTH;

    while(1) {

        UART_write(uart, "Waiting for interrupt\n", sizeof("Waiting for interrupt\n"));
        // Wait for the semaphore to post
        Semaphore_pend(getTimeSem, BIOS_WAIT_FOREVER);

        UART_write(uart, "Start I2C transfer\n", sizeof("Start I2C transfer\n"));

        if (I2C_transfer(i2c, &i2cTransaction)) {
            UART_write(uart, "I2C transfer success\n", sizeof("I2C transfer success\n"));
            for (j=0; j<RX_DATA_LENGTH; j++){
                sprintf(uartStr1, "rxBuffer at %d = ", j);
                UART_write(uart, uartStr1, sizeof(uartStr1));

                sprintf(uartStr2, "%x", rxBuffer[j]);
                UART_write(uart, uartStr2, sizeof(uartStr2));
                UART_write(uart, "\n", sizeof("\n"));
            }
            GPIO_toggle(Board_LED1);
        } else {
            System_printf("I2C Bus fault\n");
            UART_write(uart, "I2C Bus fault\n", sizeof("I2C Bus fault\n"));
        }
    }
}

/* UART initialization with parameters */
void uart_config()
{
   /* Create a UART with data processing off. */
    UART_Params_init(&uartParams);
    uartParams.writeDataMode = UART_DATA_TEXT;
    uartParams.readDataMode = UART_DATA_TEXT;
    uartParams.readReturnMode = UART_RETURN_FULL;
    uartParams.readEcho = UART_ECHO_OFF;
    uartParams.baudRate = 115200;

    uart = UART_open(Board_UART0, &uartParams);

    if (uart == NULL) {
        /* UART_open() failed */
        System_abort("Error opening the UART");
    }
}

/*
 * Initialize task with parameters
 */

void getTimeTask_init() {
    Task_Params_init(&getTimeTaskParams);
    getTimeTaskParams.stackSize = TASKSTACKSIZE;
    getTimeTaskParams.stack = &getTimeTaskStack;
    Task_construct(&getTimeTask, (Task_FuncPtr)getTimeFxn, &getTimeTaskParams, NULL);
}

void radioTask_init() {
    Task_Params_init(&radioTaskParams);
    radioTaskParams.stackSize = TASKSTACKSIZE;
    radioTaskParams.stack = &radioTaskStack;
    Task_construct(&radioTask, (Task_FuncPtr)radioFxn, &radioTaskParams, NULL);
}


/*
 *  ======== main ========
 */
int main(void)
{
    /* Call board init functions */
    Board_initGPIO();
    Board_initGeneral();
//    Board_initI2C();
    Board_initUART();
    Board_initSPI();

    uart_config();

//    Semaphore_Params params;
//    Error_Block eb;
//    /* Init params */
//    Semaphore_Params_init(&params);
//    Error_init(&eb);
//    /* Create semaphore instance */
//    bootDelaySem = Semaphore_create(0, &params, &eb);

    fastDieConfig = fastDieConfigs[fastDieSelect];

    // Turn on the radio board
    GPIO_write(POWER_RADIO, 1);
    // TURN on the power monitor GPIO
    GPIO_write(POWER_MONITOR, 1);

    // Enable interrupt on Pin 4.3
    GPIO_setConfig(CONFIG_FAST_DIE, GPIO_CFG_INPUT | GPIO_CFG_IN_PD | GPIO_CFG_IN_INT_RISING);
    /* install GPIO callback */
    GPIO_setCallback(CONFIG_FAST_DIE, setFastDieConfigFxn);
    /* Enable interrupts */
    GPIO_enableInt(CONFIG_FAST_DIE);

    // Increment lifecycle  count
    lfCount++;
    // prepare for next config change
    if(lfCount >= 10){
        lfCount = 0;
    }

//    getTimeTask_init();
    radioTask_init();

    /* Start BIOS */
    BIOS_start();

    return (0);
}
