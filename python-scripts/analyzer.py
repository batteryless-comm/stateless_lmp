import numpy as np
from collections import deque
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from numpy.lib.function_base import median

class TimeStamps:
    def __init__(self, clkFreq=8000000, timerSize=16):
        self.clkFreq = clkFreq
        self.maxTimerCount = 2**timerSize
        self.timerResolution = 1/self.clkFreq
        self.overflowResolution = self.maxTimerCount/self.clkFreq        
   
    """ 
    Calculates Time from the raw data from timer
    Input: 
        time: Time array from raw data
        ovfCount: Overflow counter of the timer
    Output:
        time in seconds
    """
    def calcTimeFromStamps(self, timeStamp, ovfCount):
        time = (self.timerResolution * timeStamp) + (self.overflowResolution* ovfCount)     
        return time

class DataParser:
    def __init__(self, fileName):
        self.fileName = fileName
        self.timestamps = TimeStamps()
        self.packetSizeBytes = 4
        self.plotter = Plotter()

    def findExpLineNumbers(self):
        expEndLineNumbers = []
        expStartLineNumbers = []
        preExpStart = 0
        file = open(self.fileName, 'r')
        # Convert the file into a list
        fileList = list(file)

        for num, line in enumerate(fileList):
            # Strip \n from each line
            stripedLine = line.rstrip("\n")
            if ("Completed" in stripedLine):
                lineNumber = num
                expEndLineNumbers.append(lineNumber)
            else:
                pass
        file.close()
        # Get the start of the experiment line numbers
        for endLine in expEndLineNumbers:
            expStartLineNumbers.append(preExpStart)
            preExpStart = endLine + 1

        return np.asarray(expStartLineNumbers)

    def getUartLinesSkip(self, numLinesSkip):
        uartLinesSkip = 6
        file = open(self.fileName, 'r')
        # Convert the file into a list
        fileList = list(file)[numLinesSkip:]
        for line in fileList:
            # Strip \n from each line
            stripedLine = line.rstrip("\n")
            # skip lines until the TX sends more than 2 packets
            if ("Power TX" in stripedLine):
                lineData = stripedLine.split(':')[1]
                data = lineData.split(',')
                numPktsTransmitted = abs(int(data[2]))
                if numPktsTransmitted >= 2:
                    break
            else:
                pass
            uartLinesSkip += 1
        file.close()
        return uartLinesSkip

    def parseExperimentsData(self):
        expDict = {}
        expLineNumbers = self.findExpLineNumbers()
        for index, lineNumber in enumerate(expLineNumbers):
            uartLinesSkip = self.getUartLinesSkip(numLinesSkip=lineNumber)
            # print("Line number: ", lineNumber)
            # print("Uart lines skip: ", uartLinesSkip)
            expDict[f"exp{index}"] = self.parseData(numLinesSkip=lineNumber+uartLinesSkip)
        
        return expDict   

    def parseDividedExpData(self):
        expDict = {}
        expLineNumbers = self.findExpLineNumbers()
        for index, lineNumber in enumerate(expLineNumbers):
            uartLinesSkip = self.getUartLinesSkip(numLinesSkip=lineNumber)
            # print("Line number: ", lineNumber)
            # print("Uart lines skip: ", uartLinesSkip)
            expDict[f"exp{index}"] = self.parseData(numLinesSkip=lineNumber+uartLinesSkip)
        
        return expDict   


    """ 
    Parse data from the LMP experiments log file
    Input: 
    Output:
        txRxData - array containing the sender and receiver stats with time, 
        onOffTimeTx - Sender's on and off times combined
        onOffTimeRx - Receiver's on and off times combined,
        numPktsTxLfTime - An array of number of packets transmitted in each lifetime,
        numPktRxLfTime - An array of number of packets received in each lifetime,
        numPktsAckLfTime - An array of number of acks received in each lifetime,
    """

    def parseData(self, numLinesSkip):
        file = open(self.fileName, 'r')
        # Convert the file into a list
        fileList = list(file)[numLinesSkip:]

        totalTime, timeAfterPrevTimeStamp = 0, 0
        prevTxTime, prevRxTime = 0, 0
        onOffTimeTx, onOffTimeRx, numPktsTxLfTime, numPktsAckLfTime, numPktRxLfTime = deque([]), deque([]), deque([]), deque([]), deque([])
        totalNumPktsTransmitted, totalNumAcks, totalPktReceived = 0, 0, 0
        txRxData, timeEdgeTx, timeEdgeRx = deque([]), deque([]), deque([])

        expDividedData = {}
        divideExpTime = 1800 # 30 minutes
        n = 1

        for line in fileList:
            # Strip \n from each line
            stripedLine = line.rstrip("\n")

            if ("Power RX" in stripedLine):
                lineData = stripedLine.split(':')[1]
                data = lineData.split(',')
                # Get the data from line into vars
                timerValue = int(data[0], base=16)
                overflowCount = abs(int(data[1]))
                pktReceived = abs(int(data[2]))
                ackSent = abs(int(data[3]))

                # Add counts to the list
                numPktRxLfTime.append(pktReceived)
                # Update the total counts of packet received
                totalPktReceived += pktReceived
                # Find the actual time value from the timer data
                timeAfterPrevTimeStamp = self.timestamps.calcTimeFromStamps(timerValue, overflowCount)
                totalTime += timeAfterPrevTimeStamp
                # Calculate time difference from prev Power TX edge
                timeDiff = totalTime - prevRxTime
                onOffTimeRx.append(timeDiff)
                prevRxTime = totalTime
                # record time at each edge of Rx
                timeEdgeRx.append(totalTime)
                # append the RX information to a common array with time
                txRxData.append([totalTime, 0, 0, 0, pktReceived, timeDiff, ackSent])

            elif ("Power TX" in stripedLine):
                lineData = stripedLine.split(':')[1]
                data = lineData.split(',')
                # Get the data from line into vars
                timerValue = int(data[0], base=16)
                overflowCount = abs(int(data[1]))
                # numComps = abs(int(data[2]))
                numPktsTransmitted = abs(int(data[2]))
                numAcks = abs(int(data[3]))
                # Add counts to the list
                # compPerLfTime.append(numComps)
                numPktsTxLfTime.append(numPktsTransmitted)
                numPktsAckLfTime.append(numAcks)
                # Update the total counts
                # totalNumComps += numComps
                totalNumPktsTransmitted += numPktsTransmitted
                totalNumAcks += numAcks
                # print(timerValue, overflowCount, numComps, numPktsTransmitted, numAcks)
                # Find the actual time value from the timer data (seconds)
                timeAfterPrevTimeStamp = self.timestamps.calcTimeFromStamps(timerValue, overflowCount)
                totalTime += timeAfterPrevTimeStamp                
                # Calculate time difference from prev Power TX edge
                timeDiff = totalTime - prevTxTime
                onOffTimeTx.append(timeDiff)
                prevTxTime = totalTime
                # record time at each edge of Rx
                timeEdgeTx.append(totalTime)
                # append the TX information to a common array with time
                txRxData.append([totalTime, timeDiff, numPktsTransmitted, numAcks, 0, 0, 0])

            elif "Completed" in stripedLine:
                lineData = stripedLine.split(':')[1]
                print(f"Experiment {lineData} data parsing completed.")
                break

            # print(totalTime)
            if (totalTime>=n*divideExpTime):
                # print(f"{totalTime} minutes completed")
                expDividedData[f"expDiv{n-1}"] = [np.asarray(txRxData), np.asarray(onOffTimeTx), np.asarray(onOffTimeRx), np.asarray(numPktsTxLfTime), np.asarray(numPktRxLfTime), np.asarray(numPktsAckLfTime), np.asarray(timeEdgeTx), np.asarray(timeEdgeRx)]
                n = n + 1
                # break

            else:
                pass
        
        file.close()

        return [np.asarray(txRxData), np.asarray(onOffTimeTx), np.asarray(onOffTimeRx), np.asarray(numPktsTxLfTime), np.asarray(numPktRxLfTime), np.asarray(numPktsAckLfTime), np.asarray(timeEdgeTx), np.asarray(timeEdgeRx), expDividedData]

    def segregateOnOffTimes(self, onOffTimeTx, onOffTimeRx, numPktsTxLfTime, numPktRxLfTime, numPktsAckLfTime, timeEdgeTx, timeEdgeRx):
        # Convert on times from seconds to ms
        onOffTimeTx = np.multiply(onOffTimeTx, 1000)
        onOffTimeRx = np.multiply(onOffTimeRx, 1000)

        onTimesTx, offTimesTx, onTimesPktSent, offTimesPktSent, onTimesAckRx, offTimesAckRx, onTimesRx, offTimesRx, onTimesPktRx, offTimesPktRx, fallingEdgeTx, risingEdgeTx, fallingEdgeRx, risingEdgeRx = deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([])

        # print(onOffTimeTx.shape)
        # print(onOffTimeRx.shape)
        # print(numPktsTxLfTime.shape)
        # print(numPktRxLfTime.shape)
        # print(numPktsAckLfTime.shape)

        # Assuming on-time is always less than off-time
        for idx in range(0, len(onOffTimeTx)-1):
            if (onOffTimeTx[idx] < onOffTimeTx[idx+1]): 
                onTimesTx.append(onOffTimeTx[idx])
                onTimesPktSent.append(numPktsTxLfTime[idx]) 
                onTimesAckRx.append(numPktsAckLfTime[idx]) 
                # falling edge of on-time
                fallingEdgeTx.append(timeEdgeTx[idx])                        
            else:
                offTimesTx.append(onOffTimeTx[idx])
                offTimesPktSent.append(numPktsTxLfTime[idx])  
                offTimesAckRx.append(numPktsAckLfTime[idx])  
                # rising edge of on-time
                risingEdgeTx.append(timeEdgeTx[idx])   

        for idx in range(0, len(onOffTimeRx)-1):
            if (onOffTimeRx[idx] < onOffTimeRx[idx+1]): 
                onTimesRx.append(onOffTimeRx[idx])
                onTimesPktRx.append(numPktRxLfTime[idx])
                # falling edge of Rx
                fallingEdgeRx.append(timeEdgeRx[idx])
            else:
                offTimesRx.append(onOffTimeRx[idx])
                offTimesPktRx.append(numPktRxLfTime[idx])
                # rising edge of Rx
                risingEdgeRx.append(timeEdgeRx[idx])

        return np.asarray(onTimesTx), np.asarray(offTimesTx), np.asarray(onTimesPktSent), np.asarray(offTimesPktSent), np.asarray(onTimesAckRx), np.asarray(offTimesAckRx), np.asarray(onTimesRx), np.asarray(offTimesRx), np.asarray(onTimesPktRx), np.asarray(offTimesPktRx), np.asarray(risingEdgeTx), np.asarray(fallingEdgeTx), np.asarray(risingEdgeRx), np.asarray(fallingEdgeRx)
    # def segregateOnOffTimes(self, onOffTimeTx, onOffTimeRx, numPktsTxLfTime,numPktRxLfTime, numPktsAckLfTime):
    #     # Convert on times from seconds to ms
    #     onOffTimeTx = np.multiply(onOffTimeTx, 1000)
    #     onOffTimeRx = np.multiply(onOffTimeRx, 1000)

    #     # Seperate on-times and off-times for both tx and
    #     #Check if first element is on time 
    #     if (onOffTimeTx[0] < onOffTimeTx[1]): 
    #         onTimesTx = onOffTimeTx[::2]
    #         offTimesTx = onOffTimeTx[1::2]
    #         onTimesNumPktsTx = numPktsTxLfTime[::2]
    #         offTimesNumPktsTx = numPktsTxLfTime[1::2]
    #         onTimesNumAcks = numPktsAckLfTime[::2]
    #         offTimesNumAcks = numPktsAckLfTime[1::2]
    #     else:
    #         onTimesTx = onOffTimeTx[1::2]
    #         offTimesTx = onOffTimeTx[::2]
    #         onTimesNumPktsTx = numPktsTxLfTime[1::2]
    #         offTimesNumPktsTx = numPktsTxLfTime[::2]
    #         onTimesNumAcks = numPktsAckLfTime[1::2]
    #         offTimesNumAcks = numPktsAckLfTime[::2]
    #     # similarily on Rx side
    #     if (onOffTimeRx[0] < onOffTimeRx[1]):
    #         onTimesRx = onOffTimeRx[::2]
    #         offTimesRx = onOffTimeRx[1::2]
    #         onTimesNumPktsRx = numPktRxLfTime[::2]
    #         offTimesNumPktsRx = numPktRxLfTime[1::2]
    #     else:
    #         onTimesRx = onOffTimeRx[1::2]
    #         offTimesRx = onOffTimeRx[::2]
    #         onTimesNumPktsRx = numPktRxLfTime[1::2]
    #         offTimesNumPktsRx = numPktRxLfTime[::2]

    #     return onTimesTx, offTimesTx, onTimesNumPktsTx, offTimesNumPktsTx, onTimesNumAcks, offTimesNumAcks, onTimesRx, offTimesRx, onTimesNumPktsRx, offTimesNumPktsRx

    def calcSuccPktSent(self, txRxData):
        _, pktsTx = self.getPktsTxwithTS(txRxData)
        totalNumPktsTransmitted = np.sum(pktsTx)
        _, pktsAck = self.getPktsAckwithTS(txRxData)
        totalNumAcks = np.sum(pktsAck)
        succPktTxPercent = (totalNumAcks/totalNumPktsTransmitted) * 100

        return succPktTxPercent
    
    def printStats(self, txRxData, onTimesTx, offTimesTx, onTimesNumPktsTx, offTimesNumPktsTx, onTimesNumAcks, offTimesNumAcks, onTimesRx, offTimesRx, onTimesNumPktsRx, offTimesNumPktsRx):
        _, pktsTx = self.getPktsTxwithTS(txRxData)
        totalNumPktsTransmitted = np.sum(pktsTx)
        _, pktsRx = self.getPktsRxwithTS(txRxData)
        totalPktReceived = np.sum(pktsRx)
        _, pktsAck = self.getPktsAckwithTS(txRxData)
        totalNumAcks = np.sum(pktsAck)
        # totalNumAcks = np.sum(onTimesNumAcks)
        # totalNumPktsTransmitted = np.sum(onTimesNumPktsTx)
        # totalPktReceived = np.sum(onTimesNumPktsRx)
        succPktTxPercent = (totalNumAcks/totalNumPktsTransmitted) * 100
        avgInterArrivalTime, interArrivalTime95th, interArrivalTime99th, interArrivalTime100th = self.interArrivalTime(txRxData)
        # print Statistics
        print("Number of on-times TX: ", np.size(onTimesTx), "\n")
        print("Number of off-times TX: ", np.size(offTimesTx), "\n")
        print("Number of on-times RX: ", np.size(onTimesRx), "\n")
        print("Number of off-times RX: ", np.size(offTimesRx), "\n")
        print("Total number of packets transmitted - TX: ", totalNumPktsTransmitted, "\n")
        print("Total number of acks received - TX: ", totalNumAcks, "\n")
        print("Total number of packets received - RX: ", totalPktReceived, "\n")
        print("Successful packet transmission %: ", succPktTxPercent, "\n")
        print("Average On-time TX: ", np.mean(onTimesTx), " ms\n")
        print("Average Off-time TX: ", np.mean(offTimesTx), " ms\n")

        avgTxDutyCycle = (np.mean(onTimesTx)/np.mean(offTimesTx))*100
        avgRxDutyCycle = (np.mean(onTimesRx)/np.mean(offTimesRx))*100
        print("Average life cycle % - Tx: ", avgTxDutyCycle, "\n")

        print("Average On-time RX:", np.mean(onTimesRx), " ms\n")
        print("Average Off-times RX:", np.mean(offTimesRx), " ms\n")
        print("Average life cycle % - Rx:", avgRxDutyCycle, " ms\n")

        print("Average inter arrival time: ", avgInterArrivalTime, "seconds\n")
        print("95th percentile inter arrival time: ", interArrivalTime95th, "seconds\n")
        print("99th percentile inter arrival time: ", interArrivalTime99th, "seconds\n")
        print("100th percentile inter arrival time: ", interArrivalTime100th, "seconds\n")

    def calcAvgLf(self, onTimesTx, onTimesRx, offTimesTx, offTimesRx):
        avgTxLifeCycle = (np.mean(onTimesTx)/(np.mean(onTimesTx)+np.mean(offTimesTx)))*100
        avgRxLifeCycle = (np.mean(onTimesRx)/(np.mean(onTimesRx)+np.mean(offTimesRx)))*100
        return avgTxLifeCycle, avgRxLifeCycle

    def interArrivalTime(self, txRxData):
        succCommTimeArr = txRxData[np.where(txRxData[:,3]>0), 0][0]
        # print(succCommTimeArr)
        interArrivalTime = np.diff(succCommTimeArr)
        # print(interArrivalTime)
        if interArrivalTime.size:
            avgInterArrivalTime = np.mean(interArrivalTime)
            medInterArrivalTime = np.median(interArrivalTime)
            interArrivalTime95th = np.percentile(interArrivalTime, 95)
            interArrivalTime99th = np.percentile(interArrivalTime, 99)
            interArrivalTime100th = np.percentile(interArrivalTime, 100)
        else:
            print("interArrivalTime array size is 0")
            avgInterArrivalTime, medInterArrivalTime, interArrivalTime95th, interArrivalTime99th, interArrivalTime100th = 0, 0, 0, 0, 0    
            

        return avgInterArrivalTime, medInterArrivalTime, interArrivalTime95th, interArrivalTime99th, interArrivalTime100th, interArrivalTime

    def getPktsTxwithTS(self, txRxData):        
        # pkts tx'ed with time stamps
        pktsTxTS = txRxData[np.where(txRxData[:,2]>=1), 0][0]
        pktsTx = txRxData[np.where(txRxData[:,2]>=1), 2][0]
        return pktsTxTS, pktsTx
    
    def getPktsAckwithTS(self, txRxData):
        # pkts ack'ed with time stamps
        pktsAckTS = txRxData[np.where(txRxData[:,3]>=2), 0][0]
        pktsAck = txRxData[np.where(txRxData[:,3]>=2), 3][0]
        return pktsAckTS, pktsAck

    def getPktsRxwithTS(self, txRxData):
        # pkts Rx'ed with time stamps
        pktsRxTS = txRxData[np.where(txRxData[:,4]>=2), 0][0]
        pktsRx = txRxData[np.where(txRxData[:,4]>=2), 4][0]
        return pktsRxTS, pktsRx

    def getAcksSentwithTS(self, txRxData):
        # acks sent with time stamps
        ackSentTS = txRxData[np.where(txRxData[:,6]>=2), 0][0]
        ackSent = txRxData[np.where((txRxData[:,6]>=1) & (txRxData[:,4]>=1)), 6][0]
        ackReceived = txRxData[np.where((txRxData[:,6]>=1) & (txRxData[:,4]>=1)), 3][0]
        return ackSentTS, ackSent, ackReceived

    def getPktsTxSuccOntime(self, txRxData):
        # pkts tx when there is successful communication
        pktsAckSuccOntime = txRxData[np.where(txRxData[:,3]>=2), 3][0]
        numSuccOnTime = len(pktsAckSuccOntime)
        medPktsAckSuccOntime = np.median(pktsAckSuccOntime)
        meanPktsAckSuccOntime = np.mean(pktsAckSuccOntime)
        return medPktsAckSuccOntime, meanPktsAckSuccOntime, numSuccOnTime

    def calcThroughput(self, txRxData, bytesPerHour=True):
        # txRxData = [totalTime, onoffTimeTx, numPktsTransmitted, numAcks, pktsRx, onoffTimeRx]
        pktsAckBw = deque([])
        pktsTxBw = deque([])
        pktsRxBw = deque([])

        # Get packets tx, rx, and ack'ed with timestamps
        pktsTxTS, pktsTx = self.getPktsTxwithTS(txRxData)
        pktsRxTS, pktsRx = self.getPktsRxwithTS(txRxData)
        pktsAckTS, pktsAck = self.getPktsAckwithTS(txRxData)

        if bytesPerHour:
            endTime = 3600
            inc = 3600
        else:
            endTime = 60
            inc = 60
        
        initEndTime = endTime
        startTime = 0

        # calculate packets ack'ed BW
        if  pktsAckTS.size:
            while endTime < pktsAckTS[-1]:
                pktsPerTime = np.sum(pktsAck[np.where((pktsAckTS>=startTime) & (pktsAckTS<endTime))])
                bytesPerTime = self.packetSizeBytes*pktsPerTime
                # print(pktsPerTime, bytesPerTime)
                pktsAckBw.append(bytesPerTime)
                startTime = endTime
                endTime += inc
                
                pktsPerTime = 0
                bytesPerTime = 0
        else:
            pktsAckBw = 0
        
        startTime = 0
        endTime = initEndTime
        # pktsPerTime = 0
        # bytesPerTime = 0

        # Calculate packets transmitted BW
        if  pktsTxTS.size:
            while endTime < pktsTxTS[-1]:
                pktsPerTime = np.sum(pktsTx[np.where((pktsTxTS>=startTime) & (pktsTxTS<endTime))])
                bytesPerTime = self.packetSizeBytes*pktsPerTime
                pktsTxBw.append(bytesPerTime)
                startTime = endTime
                endTime += inc
                
                pktsPerTime = 0
                bytesPerTime = 0

        startTime = 0
        endTime = initEndTime
        # pktsPerTime = 0
        # bytesPerTime = 0
        
        # Calculate packets Received BW
        if  pktsRxTS.size:
            while endTime < pktsRxTS[-1]:
                pktsPerTime = np.sum(pktsRx[np.where((pktsRxTS>=startTime) & (pktsRxTS<endTime))])
                bytesPerTime = self.packetSizeBytes*pktsPerTime
                pktsRxBw.append(bytesPerTime)
                startTime = endTime
                endTime += inc
                
                pktsPerTime = 0
                bytesPerTime = 0
        else:
            pktsRxBw = 0

        # calculate average BW
        avgPktsTxBW = np.mean(pktsTxBw)
        avgPktsRxBW = np.mean(pktsRxBw)
        avgPktsAckBW = np.mean(pktsAckBw)
        medPktsAckBW = np.median(pktsAckBw)
        
        return avgPktsTxBW, avgPktsRxBW, avgPktsAckBW, medPktsAckBW, pktsAckBw

    def calcNumMissingAcks(self, txRxData):
        _, numAcksSent, numAcksReceived = self.getAcksSentwithTS(txRxData)
        # _, numAcksReceived = self.getPktsAckwithTS(txRxData)
        print(numAcksReceived)
        print(numAcksSent)
        # print(np.subtract(numAcksSent, numAcksReceived))

    def calcOnOffTimeStats(self, onTimesTx, offTimesTx, onTimesNumPktsTx, offTimesNumPktsTx, onTimesNumAcks, offTimesNumAcks, onTimesRx, offTimesRx, onTimesNumPktsRx, offTimesNumPktsRx):
        # largerOnTimes = onTimesTx[np.where(onTimesTx>400)]
        noTxOnTimes = onTimesTx[np.where(onTimesNumPktsTx<=0)]
        onTimesOverlap =  onTimesTx[np.where(onTimesNumAcks>=1)]
        # numAcks = onTimesNumAcks[np.where(onTimesNumAcks>=1)]
        # largerOffTimes = offTimesTx[np.where(offTimesTx>900)]
        # print(onTimesTx[0:10])
        totalOnTime = np.sum(onTimesTx)
        totalNumAcks = np.sum(onTimesNumAcks)
        succOverlapPercent = ((totalNumAcks*3)/totalOnTime) * 100
        # print(totalNumAcks, totalOnTime, succOverlapPercent

        numOfOnTimes = len(onTimesTx)
        # numOfOnTimesOverlap = len(onTimesOverlap)
        # succOverlapPercent = (numOfOnTimesOverlap/numOfOnTimes)*100
        noTxOnTimesPercent = (len(noTxOnTimes)/numOfOnTimes)*100

        return succOverlapPercent, noTxOnTimesPercent, totalOnTime

    def calcOverlap(self, risingEdgeTx, fallingEdgeTx, risingEdgeRx, fallingEdgeRx):
        overlapTimeList = deque([])
        # ignore first element if it's off-time
        if risingEdgeTx[0] > fallingEdgeTx[0]:
            fallingEdgeTx = fallingEdgeTx[1:]
        if risingEdgeRx[0] > fallingEdgeRx[0]:
            fallingEdgeRx = fallingEdgeRx[1:]

        # find an edge of rx when tx is on
        for idx in range(len(risingEdgeTx)-1):
            # Check if there is rising edge of Rx in between on-times of tx
            condToFindRisingEdgeRx = np.where((risingEdgeTx[idx] < risingEdgeRx) & (fallingEdgeTx[idx] > risingEdgeRx))
            # Check if there is falling edge of Rx in between on-times of tx
            condToFindFallingEdgeRx = np.where((risingEdgeTx[idx] < fallingEdgeRx) & (fallingEdgeTx[idx] > fallingEdgeRx))

            # If there is a rising edge of Rx when Tx is on
            if len(risingEdgeRx[condToFindRisingEdgeRx]) >= 1:
                # print("Rising Edge Rx = ", risingEdgeRx[condToFindRisingEdgeRx][0])
                # print(fallingEdgeTx[idx] , risingEdgeRx[condToFindRisingEdgeRx][0])
                # check if the falling edge was tx/rx
                if fallingEdgeTx[idx] < fallingEdgeRx[condToFindRisingEdgeRx[0]][0]:
                    overlapTime = fallingEdgeTx[idx] - risingEdgeRx[condToFindRisingEdgeRx][0]
                    # print("Overlap = ", overlapTime)
                    overlapTimeList.append(overlapTime)
                else:
                    overlapTime = fallingEdgeRx[condToFindRisingEdgeRx][0] - risingEdgeRx[condToFindRisingEdgeRx][0]
                    # print("Overlap = ", overlapTime)
                    overlapTimeList.append(overlapTime)
            # If there is a faling edge of Rx when Tx is on
            if len(fallingEdgeRx[condToFindFallingEdgeRx] >= 1):
                # print("Falling edge Rx = ", fallingEdgeRx[condToFindFallingEdgeRx][0])
                # print(fallingEdgeRx[condToFindFallingEdgeRx][0], risingEdgeTx[idx])
                # check if the rising edge was tx/rx
                if risingEdgeTx[idx] > risingEdgeRx[condToFindFallingEdgeRx[0]]:
                    overlapTime = fallingEdgeRx[condToFindFallingEdgeRx][0] - risingEdgeTx[idx]
                    # print("Overlap = ", overlapTime)
                    overlapTimeList.append(overlapTime)
                else:
                    overlapTime = fallingEdgeRx[condToFindFallingEdgeRx][0] - risingEdgeRx[condToFindFallingEdgeRx[0]]
                    # print("Overlap = ", overlapTime)
                    overlapTimeList.append(overlapTime)
            
            
        # when tx is completely enclosed in rx on-time
        for idx in range(len(risingEdgeRx)-1):
            # Check if there is rising edge of Tx in between on-times of Rx
            condToFindRisingEdgeTx = np.where((risingEdgeRx[idx] < risingEdgeTx) & (fallingEdgeRx[idx] > risingEdgeTx))
            # Check if there is falling edge of Tx in between on-times of Rx
            condToFindFallingEdgeTx = np.where((risingEdgeRx[idx] < fallingEdgeTx) & (fallingEdgeRx[idx] > fallingEdgeTx))
            
            # If there is a rising and falling edge of Tx when Rx is on
            if len(risingEdgeTx[condToFindRisingEdgeTx]) >= 1 and fallingEdgeRx[idx] > fallingEdgeTx[condToFindRisingEdgeTx[0]]:
                overlapTime = risingEdgeTx[condToFindRisingEdgeTx[0]] - fallingEdgeTx[condToFindRisingEdgeTx[0]]
                overlapTimeList.append(overlapTime)

        # Check and make both arrays equal in length
        if len(fallingEdgeTx) > len(risingEdgeTx):
            fallingEdgeTx = fallingEdgeTx[0:len(risingEdgeTx)]
        else:
            risingEdgeTx = risingEdgeTx[0:len(fallingEdgeTx)]

        totalOverlapTime = np.sum(overlapTimeList)[0]
        totalOnTimeTx = np.sum(np.subtract(fallingEdgeTx, risingEdgeTx))
        # print(totalOverlapTime, totalOnTimeTx)
        succOverlapPercent = (totalOverlapTime/totalOnTimeTx)*100

        return succOverlapPercent
        

    def runAllExpCalc(self, expDict, plotLfData=False, plotBW=False, plotTimeData=True, printStats=True, bytesPerHour=True):
        succRate, txLifeCycle, rxLifeCycle, intArrivalTime, throughput, onTimesTxExp, onTimesRxExp, offTimesTxExp, offTimesRxExp, succOverlapPercentExp, noTxOnTimesPercentExp, numPktsTxExp, totalOnTimeExp, overlapTimeExp, medPktsAckSuccOntimeExp, numSuccOnTimeExp, meanPktsAckSuccOntimeExp, medOnTimesTxExp, medOnTimesRxExp = deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([])

        for key in expDict.keys():
            # print(expDict[key].shape)
            txRxData = expDict[key][0]
            onOffTimeTx = expDict[key][1]
            onOffTimeRx = expDict[key][2]
            numPktsTxLfTime = expDict[key][3]
            numPktRxLfTime = expDict[key][4]
            numPktsAckLfTime = expDict[key][5]
            timeEdgeTx = expDict[key][6]
            timeEdgeRx = expDict[key][7]

            #segregate on/off times of tx and rx
            onTimesTx, offTimesTx, onTimesNumPktsTx, offTimesNumPktsTx, onTimesNumAcks, offTimesNumAcks, onTimesRx, offTimesRx, onTimesNumPktsRx, offTimesNumPktsRx, risingEdgeTx, fallingEdgeTx, risingEdgeRx, fallingEdgeRx = self.segregateOnOffTimes(onOffTimeTx, onOffTimeRx, numPktsTxLfTime, numPktRxLfTime, numPktsAckLfTime, timeEdgeTx, timeEdgeRx)
            onTimesTxExp.append(onTimesTx)
            medOnTimesTxExp.append(np.median(onTimesTx))
            onTimesRxExp.append(onTimesRx)
            medOnTimesRxExp.append(np.median(onTimesRx))
            offTimesTxExp.append(offTimesTx)
            offTimesRxExp.append(offTimesRx)
            numPktsTxExp.append(np.sum(onTimesNumPktsTx))

            # calculate on/off times stats
            succOverlapPercent, noTxOnTimesPercent, totalOnTime = self.calcOnOffTimeStats(onTimesTx, offTimesTx, onTimesNumPktsTx, offTimesNumPktsTx, onTimesNumAcks, offTimesNumAcks, onTimesRx, offTimesRx, onTimesNumPktsRx, offTimesNumPktsRx)
            noTxOnTimesPercentExp.append(noTxOnTimesPercent)
            totalOnTimeExp.append(totalOnTime)

            # calculate number of successful transmission on-times and median packets successful pkts sent during successful on-times
            medPktsAckSuccOntime, meanPktsAckSuccOntime, numSuccOnTime = self.getPktsTxSuccOntime(txRxData)
            medPktsAckSuccOntimeExp.append(medPktsAckSuccOntime)
            meanPktsAckSuccOntimeExp.append(meanPktsAckSuccOntime)
            numSuccOnTimeExp.append(numSuccOnTime)

            # calculate overlap time
            # succOverlapPercent = self.calcOverlap(risingEdgeTx, fallingEdgeTx, risingEdgeRx, fallingEdgeRx)
            succOverlapPercentExp.append(succOverlapPercent)
      
            if printStats:
                print(f"\n***********Experiment {key} stats:******************\n")
                self.printStats(txRxData, onTimesTx, offTimesTx, onTimesNumPktsTx, offTimesNumPktsTx, onTimesNumAcks, offTimesNumAcks, onTimesRx, offTimesRx, onTimesNumPktsRx, offTimesNumPktsRx)

            # Calculate inter arrival time
            avgInterArrivalTime, _, interArrivalTime95th, interArrivalTime99th, interArrivalTime100th, _ = self.interArrivalTime(txRxData)
            intArrivalTime.append([avgInterArrivalTime, interArrivalTime95th, interArrivalTime99th, interArrivalTime100th])
            # append lifecycle of each experiment [sender, receiver]
            senderLf, receiverLf = self.calcAvgLf(onTimesTx, onTimesRx, offTimesTx, offTimesRx)
            txLifeCycle.append(senderLf)
            rxLifeCycle.append(receiverLf)
            # append success rate of each experiment
            succRate.append(self.calcSuccPktSent(txRxData))
            pktsTxBW, pktsRxBW, pktsAckBW, _, _ = self.calcThroughput(txRxData, bytesPerHour=bytesPerHour)
            throughput.append([pktsTxBW, pktsRxBW, pktsAckBW])

            if plotBW:
                self.plotter.plotBW(np.mean(pktsTxBW), np.mean(pktsRxBW), np.mean(pktsAckBW), expNo=key, bytesPerHour=bytesPerHour, printToPdf=False)
            # plot lifecycle data plots
            if plotLfData:
                self.plotter.plotBW(np.mean(pktsTxBW), np.mean(pktsRxBW), np.mean(pktsAckBW), bytesPerHour, printToPdf=False)
                self.plotter.plotSenderStatvsLifecycle(onTimesTx, onTimesNumPktsTx, onTimesNumAcks, numOfLifeCycletoPlot=100, printToPdf=False)
                self.plotter.plotReceiverStatvsLifecycle(onTimesRx, onTimesNumPktsRx, numOfLifeCycletoPlot=100, printToPdf=False)
            # plot time data plots
            if plotTimeData:
                self.plotter.plotStatsVsTime(txRxData, startTime=0, endTime=7400, printToPdf=False)

            # avgTxLifeCycle = np.mean(txLifeCycle)
            # avgRxLifeCycle = np.mean(rxLifeCycle)
        # return np.asarray(succRate), np.asarray(avgTxLifeCycle), np.asarray(avgRxLifeCycle), np.asarray(intArrivalTime), np.asarray(throughput), np.asarray(onTimesTxExp), np.asarray(onTimesRxExp), np.asarray(offTimesTxExp), np.asarray(offTimesRxExp)
        return np.asarray(succRate), np.asarray(txLifeCycle), np.asarray(rxLifeCycle), np.asarray(intArrivalTime), np.asarray(throughput), onTimesTxExp, onTimesRxExp, offTimesTxExp, offTimesRxExp, np.asarray(succOverlapPercentExp), np.asarray(noTxOnTimesPercentExp), np.asarray(numPktsTxExp), np.asarray(totalOnTimeExp), medPktsAckSuccOntimeExp, numSuccOnTimeExp, meanPktsAckSuccOntimeExp, medOnTimesTxExp, medOnTimesRxExp
    
    def runSweepExpCalc(self, expDict, printStats=False, bytesPerHour=False):
        intArrivalTime, throughput, medOnTimesTxExp, medOnTimesRxExp, lmpConfigTxRx, txLifeCycleExp, rxLifeCycleExp, minOnTimesTxList, maxOnTimesTxList, minOnTimesRxList, maxOnTimesRxList, avgOnTimesTxList, avgOnTimesRxList, medOnTimesTxList, medOnTimesRxList, medNullOnTimesTx, medUnsuccLMP1OnTimesTx, expMapModelTTOsList, expMapModelThroughputsList = deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([])
        for key in expDict.keys():
            txRxData = expDict[key][0]
            onOffTimeTx = expDict[key][1]
            onOffTimeRx = expDict[key][2]
            numPktsTxLfTime = expDict[key][3]
            numPktRxLfTime = expDict[key][4]
            numPktsAckLfTime = expDict[key][5]
            timeEdgeTx = expDict[key][6]
            timeEdgeRx = expDict[key][7]

            #segregate on/off times of tx and rx
            onTimesTx, offTimesTx, _, _, _, _, onTimesRx, offTimesRx, _, _, _, _, _, _ = self.segregateOnOffTimes(onOffTimeTx, onOffTimeRx, numPktsTxLfTime, numPktRxLfTime, numPktsAckLfTime, timeEdgeTx, timeEdgeRx)
            medOnTimesTxExp.append(np.median(onTimesTx))
            medOnTimesRxExp.append(np.median(onTimesRx))  

            # print("On time of tx: ", onTimesTx)
            # print(onTimesTx)
            # print(np.min(onTimesTx), np.max(onTimesTx))
            minOnTimesTxList.append(np.min(onTimesTx))
            maxOnTimesTxList.append(np.max(onTimesTx))
            avgOnTimesTxList.append(np.mean(onTimesTx))
            medOnTimesTxList.append(np.median(onTimesTx))
            minOnTimesRxList.append(np.min(onTimesRx))
            maxOnTimesRxList.append(np.max(onTimesRx))
            avgOnTimesRxList.append(np.mean(onTimesRx))
            medOnTimesRxList.append(np.median(onTimesRx))
            
            # calculate average lifecycle  
            senderLf, receiverLf = self.calcAvgLf(onTimesTx, onTimesRx, offTimesTx, offTimesRx)
            txLifeCycleExp.append(senderLf)
            rxLifeCycleExp.append(receiverLf)

            print(f"{key} lifecycles:", senderLf, receiverLf)

            # Get inter arrival time
            avgInterArrivalTime, _, interArrivalTime95th, interArrivalTime99th, interArrivalTime100th, _ = self.interArrivalTime(txRxData)
            intArrivalTime.append(avgInterArrivalTime)

            # Get throughput
            pktsTxBW, pktsRxBW, pktsAckBW, _, _ = self.calcThroughput(txRxData, bytesPerHour=bytesPerHour)
            pktsAckBW = np.divide(pktsAckBW, 1024)*60
            throughput.append(pktsAckBW)  # kB/hr

            #  Get number of missing acks
            # self.calcNumMissingAcks(txRxData)

            # get LMP configs
            txLMPConfig = self.plotter.getTxLMPConfig(np.median(onTimesTx))
            rxLMPConfig = self.plotter.getRxLMPConfig(np.median(onTimesRx))
            lmpConfigTxRx.append([txLMPConfig, rxLMPConfig])

            # get points from model
            mdl = Model(senderLf/100, receiverLf/100)
            ontimes, TTOs, throughputs = mdl.calcMetrics()
            mappedTxLmpConfig = AnalysisRunner().lmpConfigMap(txLMPConfig)
            mappedRxLmpConfig = AnalysisRunner().lmpConfigMap(rxLMPConfig)
            # print(ontimes)
            # x = np.divide(ontimes-mdl.boottime, mdl.s)
            expMapModelTTOs = TTOs[mappedTxLmpConfig-1,mappedRxLmpConfig-1]
            expMapModelThroughputs = throughputs[mappedTxLmpConfig-1,mappedRxLmpConfig-1]
            expMapModelTTOsList.append(expMapModelTTOs)
            expMapModelThroughputsList.append(expMapModelThroughputs)

            # print(np.shape(np.where(txRxData[:, 2]>1)))
            
            if printStats:
                # Throughput
                # print("Throughput: ", pktsAckBW)
                # print("TTH: ", avgInterArrivalTime)
                # Calculate median of null-null config
                if txLMPConfig == 'LMP-Null' and rxLMPConfig == 'LMP-Null':
                    print(np.median(onTimesTx))
                if txLMPConfig == 'LMP-1' and rxLMPConfig == 'LMP-1':
                    print("lmp1 median on time =", np.median(onTimesTx))

                if txLMPConfig == 'LMP-Null':
                    medNullOnTimesTx.append(np.median(onTimesTx))
                if txLMPConfig == 'LMP-1':
                    # print("Median on-times LMP-1", onTimesTx)
                    # print("On-time shape = ", txRxData[(np.where(txRxData[:,2]>1))])
                    # print("Succ On-time shape = ", txRxData[(np.where(txRxData[:,3]>1))])
                    # print("On-time shape = ", onTimesTx.shape)
                    # print("Unsuccessful On-time shape = ", onTimesTx[np.where(onTimesTx<50)].shape)
                    medUnsuccLMP1OnTimesTx.append(np.median(onTimesTx[np.where(onTimesTx<40)]))
                    # print("Min on-time LMP-1 = ", np.min(onTimesTx))
                    # print("Mean on-time LMP-1 = ", np.mean(onTimesTx))

        if printStats:
            print("TX:")
            print("Minimum on-time = ", np.min(minOnTimesTxList), "Average on-time = ", np.mean(avgOnTimesTxList), "Median on-time = ", np.median(medOnTimesTxList), "Maximum on-time = ", np.max(maxOnTimesTxList))
            print("RX:")
            print("Minimum on-time = ", np.min(minOnTimesRxList), "Average on-time = ", np.mean(avgOnTimesRxList), "Median on-time = ", np.median(medOnTimesRxList), "Maximum on-time = ", np.max(maxOnTimesRxList))
            print("Median LMP-Null on-times: ", np.median(medNullOnTimesTx))
            print("Median LMP-1 unsuccessful on-times: ", np.median(medUnsuccLMP1OnTimesTx))

                
        return np.asarray(intArrivalTime), np.asarray(throughput), np.asarray(medOnTimesTxExp), np.asarray(medOnTimesRxExp), np.asarray(lmpConfigTxRx), np.asarray(txLifeCycleExp), np.asarray(rxLifeCycleExp), np.asarray(expMapModelTTOsList), np.asarray(expMapModelThroughputsList)
    
    def runAppendMetricCalc(self, expDict, medianConfig=False, meanConfig=True, bytesPerHour=False):
        intArrivalTime, intArrivalTimeArrExp, throughput, medOnTimesTxExp, medOnTimesRxExp, lmpConfigTxRx, txLifeCycleExp, rxLifeCycleExp, throughputExp = deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([]), deque([])
        for key in expDict.keys():
            txRxData = expDict[key][0]
            onOffTimeTx = expDict[key][1]
            onOffTimeRx = expDict[key][2]
            numPktsTxLfTime = expDict[key][3]
            numPktRxLfTime = expDict[key][4]
            numPktsAckLfTime = expDict[key][5]
            timeEdgeTx = expDict[key][6]
            timeEdgeRx = expDict[key][7]


            #segregate on/off times of tx and rx
            onTimesTx, offTimesTx, _, _, _, _, onTimesRx, offTimesRx, _, _, _, _, _, _ = self.segregateOnOffTimes(onOffTimeTx, onOffTimeRx, numPktsTxLfTime, numPktRxLfTime, numPktsAckLfTime, timeEdgeTx, timeEdgeRx)
            medOnTimesTxExp.append(np.median(onTimesTx))
            medOnTimesRxExp.append(np.median(onTimesRx))  
            
            # calculate average lifecycle  
            senderLf, receiverLf = self.calcAvgLf(onTimesTx, onTimesRx, offTimesTx, offTimesRx)
            txLifeCycleExp.append(senderLf)
            rxLifeCycleExp.append(receiverLf)

            # Get inter arrival time
            medianConfig = True
            meanConfig = False
            avgInterArrivalTime, medInterArrivalTime, interArrivalTime95th, interArrivalTime99th, interArrivalTime100th, interArrivalTimeArr = self.interArrivalTime(txRxData)
            if meanConfig:
                intArrivalTime.append(avgInterArrivalTime)
            elif medianConfig:
                intArrivalTime.append(medInterArrivalTime)
                
            intArrivalTimeArrExp.append(interArrivalTimeArr)

            # Get throughput
            pktsTxBW, pktsRxBW, pktsAckBW, medpktsAckBW, pktsAckBWArr = self.calcThroughput(txRxData, bytesPerHour=bytesPerHour)
            if meanConfig:
                pktsAckBW = np.divide(pktsAckBW, 1024)*60
                throughput.append(pktsAckBW)  # kB/hr
            elif medianConfig:
                medpktsAckBW = np.divide(medpktsAckBW, 1024)*60
                throughput.append(medpktsAckBW)  # kB/hr

            succthroughput = np.multiply(np.divide(pktsAckBWArr, 1024), 60)
            throughputExp.append(succthroughput)

            # get LMP configs
            txLMPConfig = self.plotter.getTxLMPConfig(np.median(onTimesTx))
            rxLMPConfig = self.plotter.getRxLMPConfig(np.median(onTimesRx))
            lmpConfigTxRx.append([txLMPConfig, rxLMPConfig])
                
        return np.asarray(intArrivalTime), np.asarray(throughput), np.asarray(medOnTimesTxExp), np.asarray(medOnTimesRxExp), np.asarray(lmpConfigTxRx), np.asarray(txLifeCycleExp), np.asarray(rxLifeCycleExp), np.asarray(intArrivalTimeArrExp), np.asarray(throughputExp)

class Model:
    def __init__(self, txLf, rxLf):
        self.use3D = False
        self.Pon = 50
        self.P0 = txLf * self.Pon
        self.P1 = rxLf * self.Pon
        self.s = 5
        self.boottime = 15
        self.Tmin = self.boottime + self.s
        self.Tmax = 250


    def expectedTTO(self, T0, T1):
	    return (self.Pon * self.Pon * T0 * T1) / (self.P0 * self.P1 * (T0 + T1 + - 2*self.boottime - self.s))
    
    def expectedThroughput(self, T0, T1):
        m = T0 // self.s
        n = T1 // self.s
        K = self.Tmax // self.s
        b = self.boottime // self.s

        X = np.sum([K - i for i in range(b, m)]) + np.sum([K - i for i in range(b+1, n)])

        return self.P0*self.P1*self.s*X / (T0*T1*self.Pon*self.Pon)

    def calcMetrics(self):
        ontimes = np.array([k for k in range(self.Tmin, self.Tmax + 1, self.s)])
        TTOs = np.array([[self.expectedTTO(T0, T1)/1000 for T1 in ontimes] for T0 in ontimes])
        throughputs = np.array([[self.expectedThroughput(T0, T1)*1000 for T1 in ontimes] for T0 in ontimes])

        return ontimes, TTOs, throughputs


class AnalysisRunner:
    def __init__(self):
        self.plotter = Plotter()
        
    def runTxSweepAnalysis(self, expFiles, senderLMPs, receiverLMPs, date, printToPdf=False):
        for idx, f in enumerate(expFiles):
            dp = DataParser(fileName=f)
            expDict = dp.parseExperimentsData()
            succPercent, avgTxLfc, avgRxLfc, avgIntArrivalTime, throughput, onTimesTxExp, onTimesRxExp, offTimesTxExp, offTimesRxExp, overlapPercentExp, noTxOnTimesPercentExp, numPktsTxExp, totalOnTimeExp,  medPktsAckSuccOntimeExp, numSuccOnTimeExp, meanPktsAckSuccOntimeExp, medOnTimesTxExp, medOnTimesRxExp = dp.runAllExpCalc(expDict, printStats=False, plotBW=False, plotTimeData=False, bytesPerHour=True)
            self.plotter.plotAggStats(succPercent, avgTxLfc, avgRxLfc, avgIntArrivalTime, throughput, onTimesTxExp, onTimesRxExp, medOnTimesTxExp, medOnTimesRxExp, offTimesTxExp, offTimesRxExp, overlapPercentExp, noTxOnTimesPercentExp, numPktsTxExp,  medPktsAckSuccOntimeExp, meanPktsAckSuccOntimeExp, numSuccOnTimeExp, expNum=idx, senderLMPs=senderLMPs, receiverLMPs=receiverLMPs[idx], date=date[idx], printToPdf=True)
            print("--------------------------------------------------")

    def runDivExpAnalysis(self, expFiles, senderLMPs, receiverLMPs, date):
        lmpConfig = ['LMP-1', 'LMP-5', 'LMP-10', 'LMP-15', 'LMP-Null']
        i=0
        for idx, f in enumerate(expFiles):
            dp = DataParser(fileName=f)
            expDict = dp.parseExperimentsData()
            for keyExp in expDict.keys():
                succPercent, avgTxLfc, avgRxLfc, avgIntArrivalTime, throughput, onTimesTxExp, onTimesRxExp, offTimesTxExp, offTimesRxExp, overlapPercentExp, noTxOnTimesPercentExp, numPktsTxExp, totalOnTimeExp,  medPktsTxSuccOntimeExp, numSuccOnTimeExp = dp.runAllExpCalc(expDict[keyExp][8], printStats=False, plotBW=False, plotTimeData=False, bytesPerHour=False)   
                self.plotter.plotExpDivAggStats(succPercent, avgTxLfc, avgRxLfc, avgIntArrivalTime, throughput, onTimesTxExp, onTimesRxExp, offTimesTxExp, offTimesRxExp, overlapPercentExp, noTxOnTimesPercentExp, numPktsTxExp,  medPktsTxSuccOntimeExp, numSuccOnTimeExp, idx, senderLMPs, receiverLMPs[idx], keyExp, lmpConfig[i], date, printToPdf=False)
                i = i+1
            print("--------------------------------------------------")
    
    def lmpConfigMap(self, lmpConfig):
        if lmpConfig == 'LMP-1':
            lmpConfigIdx = 1
        elif lmpConfig == 'LMP-5':
            lmpConfigIdx = 5
        elif lmpConfig == 'LMP-10':
            lmpConfigIdx = 10
        elif lmpConfig == 'LMP-15':
            lmpConfigIdx = 15
        elif lmpConfig == 'LMP-Null':
            lmpConfigIdx = 45
        else:
            lmpConfigIdx = 0

        return lmpConfigIdx

    def lmpConfigMapper(self, lmpConfigTxRx):
        txLMPIdxExp, rxLMPIdxExp = deque([]), deque([])
        for lmp in lmpConfigTxRx:
            txLMPIdx = self.lmpConfigMap(lmp[0])
            rxLMPIdx = self.lmpConfigMap(lmp[1])
            txLMPIdxExp.append(txLMPIdx)
            rxLMPIdxExp.append(rxLMPIdx)

        return np.asarray(txLMPIdxExp), np.asarray(rxLMPIdxExp)

    def runFullSweepAnalysis(self, expFiles, expDates, printToPdf=False):
        for idx, f in enumerate(expFiles):
            dp = DataParser(fileName=f)
            expDict = dp.parseExperimentsData()

            intArrivalTime, throughput, medOnTimesTxExp, medOnTimesRxExp, lmpConfigTxRx, txLifeCycleExp, rxLifeCycleExp, expMapModelTTOsList, expMapModelThroughputsList = dp.runSweepExpCalc(expDict, printStats=False)
            avgTxLifeCycle = np.round(np.mean(txLifeCycleExp), decimals=2)
            avgRxLifeCycle = np.round(np.mean(rxLifeCycleExp), decimals=2)

            print("Tx lifecycle = ", avgTxLifeCycle, "Rx lifecycle = ", avgRxLifeCycle)
            # print(expMapModelTTOsList)
            # print(expMapModelThroughputsList)

            txLMPIdxExp, rxLMPIdxExp = self.lmpConfigMapper(lmpConfigTxRx)
            
            print("Generating plots ...")
            if len(expDict.keys()) > 2:
                # get mathematical model data
                md = Model(avgTxLifeCycle/100, avgRxLifeCycle/100)
                ontimes, TTOs, throughputs = md.calcMetrics()

                self.plotter.plotModelvsExpMetrics(txLMPIdxExp, rxLMPIdxExp, intArrivalTime, ontimes, TTOs, throughputs, md, avgTxLifeCycle, avgRxLifeCycle, metric="inter-handshake", expDate=expDates[idx], use3D=True, printToPdf=printToPdf)
                self.plotter.plotModelvsExpMetrics(txLMPIdxExp, rxLMPIdxExp, throughput, ontimes, TTOs, throughputs, md, avgTxLifeCycle, avgRxLifeCycle, metric="throughput", expDate=expDates[idx], use3D=True, printToPdf=printToPdf)
                
                self.plotter.plot3DSurface(txLMPIdxExp, rxLMPIdxExp, intArrivalTime, avgTxLifeCycle, avgRxLifeCycle, metric="inter-handshake", expDate=expDates[idx], printToPdf=printToPdf) 
                self.plotter.plot3DSurface(txLMPIdxExp, rxLMPIdxExp, throughput, avgTxLifeCycle, avgRxLifeCycle, metric="throughput", expDate=expDates[idx], printToPdf=printToPdf) 
                
                self.plotter.plotHeatMap(txLMPIdxExp, rxLMPIdxExp, intArrivalTime, avgTxLifeCycle, avgRxLifeCycle, metric="inter-handshake", expDate=expDates[idx], printToPdf=printToPdf)
                self.plotter.plotHeatMap(txLMPIdxExp, rxLMPIdxExp, throughput , avgTxLifeCycle, avgRxLifeCycle, metric="throughput", expDate=expDates[idx], printToPdf=printToPdf)
                self.plotter.plotHeatMap(txLMPIdxExp, rxLMPIdxExp, txLifeCycleExp, avgTxLifeCycle, avgRxLifeCycle, metric="lifecycle_tx", expDate=expDates[idx], printToPdf=printToPdf)
                self.plotter.plotHeatMap(txLMPIdxExp, rxLMPIdxExp, rxLifeCycleExp, avgTxLifeCycle, avgRxLifeCycle, metric="lifecycle_rx", expDate=expDates[idx], printToPdf=printToPdf)
                self.plotter.plotHeatMap(txLMPIdxExp, rxLMPIdxExp, expMapModelTTOsList, avgTxLifeCycle, avgRxLifeCycle, metric="model-tto", expDate=expDates[idx], printToPdf=printToPdf)
                self.plotter.plotHeatMap(txLMPIdxExp, rxLMPIdxExp, expMapModelThroughputsList, avgTxLifeCycle, avgRxLifeCycle, metric="model-th", expDate=expDates[idx], printToPdf=printToPdf)

            print("Plots generated successfully!")

            # print(txLMPIdxExp)
            # print(rxLMPIdxExp)
            # print(np.asarray(intArrivalTime))
            # print(np.asarray(throughput))

            # self.plotter.plot3DScatter(txLMPIdxExp, rxLMPIdxExp, intArrivalTime, metric="inter-handshake", printToPdf=False)
            # self.plotter.plot3DScatter(txLMPIdxExp, rxLMPIdxExp, throughput, metric="throughput", printToPdf=False)    
            # self.plotter.plot2Dmetric(txLMPIdxExp, rxLMPIdxExp, intArrivalTime, ontimes, TTOs, throughputs, avgTxLifeCycle, avgRxLifeCycle, metric="inter-handshake", model=md, expDate=expDates[idx], printToPdf=printToPdf) 
            # self.plotter.plot2Dmetric(txLMPIdxExp, rxLMPIdxExp, throughput, ontimes, TTOs, throughputs, avgTxLifeCycle, avgRxLifeCycle, metric="throughput", model=md, expDate=expDates[idx], printToPdf=printToPdf) 

        return expDict

    def avgMetricData(self, metricData3D, medianConfig=False, meanConfig=True):
        avgMetricArr = np.zeros((np.shape(metricData3D)[1], np.shape(metricData3D)[2]))
        # print(metricData3D[:][:, 0, 0])
        for row in range(np.shape(metricData3D)[1]):
            for col in range(np.shape(metricData3D)[2]):
                ele3D = metricData3D[:][:, row, col]
                nonZeroIdx = np.nonzero(ele3D)
                if meanConfig:
                    avgMetricArr[row][col] = np.mean(ele3D[nonZeroIdx])
                elif medianConfig:
                    avgMetricArr[row][col] = np.median(ele3D[nonZeroIdx])

        return avgMetricArr
   
    def appendMetrics(self, expFiles, expDates, medianConfig=False, meanConfig=True, printToPdf=False):
        throughputFiles, intArrivalTimeFiles, txLMPIdxFiles, rxLMPIdxFiles, txLifeCycleFiles, rxLifeCycleFiles = [], [], [], [], [], []
        # intArrivalTimeArrFiles = np.asarray(intArrivalTimeArrFiles)
        # intArrivalTimeArrFiles = np.array((25,))
        intArrivalTimeArrFiles, throughputArrFiles = deque([]), deque([])
        for idx, f in enumerate(expFiles):
            dp = DataParser(fileName=f)
            expDict = dp.parseExperimentsData()

            intArrivalTime, throughput, medOnTimesTxExp, medOnTimesRxExp, lmpConfigTxRx, txLifeCycleExp, rxLifeCycleExp, intArrivalTimeArrExp, throughputExpArr = dp.runAppendMetricCalc(expDict, medianConfig=medianConfig, meanConfig=meanConfig)
            if meanConfig:
                avgTxLifeCycle = np.round(np.mean(txLifeCycleExp), decimals=2)
                avgRxLifeCycle = np.round(np.mean(rxLifeCycleExp), decimals=2)
                txLifeCycleFiles.append(avgTxLifeCycle)
                rxLifeCycleFiles.append(avgRxLifeCycle)
            elif medianConfig:
                medTxLifeCycle = np.round(np.median(txLifeCycleExp), decimals=2)
                medRxLifeCycle = np.round(np.median(rxLifeCycleExp), decimals=2)
                txLifeCycleFiles.append(medTxLifeCycle)
                rxLifeCycleFiles.append(medRxLifeCycle)

            txLMPIdxExp, rxLMPIdxExp = self.lmpConfigMapper(lmpConfigTxRx)
            txLMPIdxFiles.append(txLMPIdxExp)
            rxLMPIdxFiles.append(rxLMPIdxExp)

            # get mathematical model data
            md = Model(medTxLifeCycle/100, medRxLifeCycle/100)
            ontimes, TTOs, throughputs = md.calcMetrics()

            self.plotter.plot2Dmetric(txLMPIdxExp, rxLMPIdxExp, intArrivalTime, ontimes, TTOs, throughputs, medTxLifeCycle, medRxLifeCycle, metric="inter-handshake", model=md,expDate=expDates[idx], printToPdf=printToPdf) 
            self.plotter.plot2Dmetric(txLMPIdxExp, rxLMPIdxExp, throughput, ontimes, TTOs, throughputs, medTxLifeCycle, medRxLifeCycle, metric="throughput", model=md, expDate=expDates[idx], printToPdf=printToPdf) 

            self.plotter.plotHeatMap(txLMPIdxExp, rxLMPIdxExp, intArrivalTime, medTxLifeCycle, medRxLifeCycle, metric="inter-handshake", expDate=expDates[idx], printToPdf=printToPdf)
            self.plotter.plotHeatMap(txLMPIdxExp, rxLMPIdxExp, throughput , medTxLifeCycle, medRxLifeCycle, metric="throughput", expDate=expDates[idx], printToPdf=printToPdf)

            self.plotter.plot3DSurface(txLMPIdxExp, rxLMPIdxExp, intArrivalTime, medTxLifeCycle, medRxLifeCycle, metric="inter-handshake", expDate=expDates[idx], printToPdf=printToPdf) 
            self.plotter.plot3DSurface(txLMPIdxExp, rxLMPIdxExp, throughput, medTxLifeCycle, medRxLifeCycle, metric="throughput", expDate=expDates[idx], printToPdf=printToPdf) 

            self.plotter.plotModelvsExpSubplots(txLMPIdxExp, rxLMPIdxExp, intArrivalTime, ontimes, TTOs, throughputs, md, medTxLifeCycle, medRxLifeCycle, metric="inter-handshake", expDate=expDates[idx], use3D=True, printToPdf=printToPdf)
            self.plotter.plotModelvsExpSubplots(txLMPIdxExp, rxLMPIdxExp, throughput, ontimes, TTOs, throughputs, md, medTxLifeCycle, medRxLifeCycle, metric="throughput", expDate=expDates[idx], use3D=True, printToPdf=printToPdf)

            self.plotter.plotModelvsExpMetrics(txLMPIdxExp, rxLMPIdxExp, intArrivalTime, ontimes, TTOs, throughputs, md, medTxLifeCycle, medRxLifeCycle, metric="inter-handshake", expDate=expDates[idx], use3D=True, printToPdf=printToPdf)
            self.plotter.plotModelvsExpMetrics(txLMPIdxExp, rxLMPIdxExp, throughput, ontimes, TTOs, throughputs, md, medTxLifeCycle, medRxLifeCycle, metric="throughput", expDate=expDates[idx], use3D=True, printToPdf=printToPdf)

            intArrivalTimeArrFiles.append(intArrivalTimeArrExp)
            throughputArrFiles.append(throughputExpArr)

            # intArrivalTimeArrFiles.append(intArrivalTimeArrExp)
            intArrivalTimeFiles.append(self.plotter.getMetricArr(txLMPIdxExp, rxLMPIdxExp, intArrivalTime))
            throughputFiles.append(self.plotter.getMetricArr(txLMPIdxExp, rxLMPIdxExp, throughput))

        intArrivalTimeMetric = self.plotter.maptoMetricArr(txLMPIdxFiles, rxLMPIdxFiles, intArrivalTimeArrFiles, meanFlag=meanConfig)
        throughputMetric = self.plotter.maptoMetricArr(txLMPIdxFiles, rxLMPIdxFiles, throughputArrFiles, meanFlag=meanConfig)

        if meanConfig:
            txLifeCycle = np.mean(txLifeCycleFiles)
            rxLifeCycle = np.mean(rxLifeCycleFiles)
        elif medianConfig:
            txLifeCycle = np.median(txLifeCycleFiles)
            rxLifeCycle = np.median(rxLifeCycleFiles)
        
        # get mathematical model data
        md = Model(txLifeCycle/100, rxLifeCycle/100)
        ontimes, TTOs, throughputs = md.calcMetrics()

        txLMPIdx = [1, 5, 10, 15, 45, 1, 5, 10, 15, 45, 1, 5, 10, 15, 45, 1, 5, 10, 15, 45, 1, 5, 10, 15, 45]
        rxLMPIdx = np.repeat([1, 5, 10, 15, 45], 5)

        self.plotter.plot3DSurface(txLMPIdx, rxLMPIdx, intArrivalTimeMetric, txLifeCycle, rxLifeCycle, metric="inter-handshake", expDate="04272021", printToPdf=True)

        self.plotter.plot3DSurface(txLMPIdx, rxLMPIdx, throughputMetric, txLifeCycle, rxLifeCycle, metric="throughput", expDate="04272021", printToPdf=True)

        self.plotter.plot2DAppendMetric(txLMPIdxExp, rxLMPIdxExp, intArrivalTimeMetric, ontimes, TTOs, throughputs, txLifeCycle, rxLifeCycle, metric="inter-handshake", model=md, expDate="04272021", printToPdf=True)

        self.plotter.plot2DAppendMetric(txLMPIdxExp, rxLMPIdxExp, throughputMetric, ontimes, TTOs, throughputs, txLifeCycle, rxLifeCycle, metric="throughput", model=md, expDate="04272021", printToPdf=True)

    
    def runBootDelayAnalysis(self, expFiles, expDates, printToPdf=False):
        for idx, f in enumerate(expFiles):
            dp = DataParser(fileName=f)
            expDict = dp.parseExperimentsData()

            intArrivalTime, throughput, medOnTimesTxExp, medOnTimesRxExp, lmpConfigTxRx, txLifeCycleExp, rxLifeCycleExp = dp.runSweepExpCalc(expDict)
            avgTxLifeCycle = np.round(np.mean(txLifeCycleExp), decimals=2)
            avgRxLifeCycle = np.round(np.mean(rxLifeCycleExp), decimals=2)

            txLMPIdxExp, rxLMPIdxExp = self.lmpConfigMapper(lmpConfigTxRx)
            
            # self.plotter.plotBootTimeVar(txLMPIdxExp, rxLMPIdxExp, intArrivalTime, avgTxLifeCycle, avgRxLifeCycle, metric="inter-handshake", expDate=expDates[idx], printToPdf=printToPdf)
            self.plotter.plotBootTimeVar(txLMPIdxExp, rxLMPIdxExp, intArrivalTime, throughput, avgTxLifeCycle, avgRxLifeCycle, expDate=expDates[idx], printToPdf=printToPdf)

        return expDict
        


class Plotter:
    def __init__(self):
        self.onTimesTxColor = "royalblue"
        self.pktsTxColor = "coral"
        self.ackRxColor = "slategray"
        self.onTimeRxColor = "seagreen"
        self.pktRxColor = "crimson"
        self.bytesPerPkt = 4
        self.elev = 37
        self.azim = -67
        self.scaling = False

    def findLMPConfigExp(self, medOnTimesTxExp, medOnTimesRxExp):
        lmpConfigXlabelsTx = []

        for ontime in medOnTimesTxExp:
            lmpConfigXlabelsTx.append(self.getTxLMPConfig(ontime))
        
        for ontime in medOnTimesRxExp:
            rxLMPConfig = self.getRxLMPConfig(self.getRxLMPConfig(ontime))                    

        return lmpConfigXlabelsTx, rxLMPConfig

    def getTxLMPConfig(self, ontime):
        if ontime < 35:
            txLmpConfig = 'LMP-1'
        elif ontime >= 35 and ontime < 60:
            txLmpConfig = 'LMP-5'
        elif ontime >= 60 and ontime < 85:
            txLmpConfig = 'LMP-10'
        elif ontime >= 85 and ontime < 115:
            txLmpConfig = 'LMP-15'
        elif ontime >= 115:
            txLmpConfig = 'LMP-Null'
        return txLmpConfig
    
    def getRxLMPConfig(self, ontime):
        if ontime < 35:
            rxLMPConfig = 'LMP-1'
        elif ontime >= 35 and ontime < 60:
            rxLMPConfig = 'LMP-5'
        elif ontime >= 60 and ontime < 85:
            rxLMPConfig = 'LMP-10'
        elif ontime >= 85 and ontime < 120:
            rxLMPConfig = 'LMP-15'
        elif ontime >= 120:
            rxLMPConfig = 'LMP-Null'
        else:
            rxLMPConfig = 'LMP-Null'
        return rxLMPConfig    
    
    
    def lmpConfigToLabel(self, lmpConfig):
        lmpConfigLabel = []

        for lmp in lmpConfig:
            if lmp == 1:
                txLMPConfigLabel = 0
            elif lmp == 5:
                txLMPConfigLabel = 1
            elif lmp == 10:
                txLMPConfigLabel = 2
            elif lmp == 15:
                txLMPConfigLabel = 3
            elif lmp == 45:
                txLMPConfigLabel = 4
            
            lmpConfigLabel.append(txLMPConfigLabel)
        
        return lmpConfigLabel
    
    def getMetricArr(self, x, y, z):
        idx=0
        metricArr = np.zeros((5, 5))
        
        txLabel = self.lmpConfigToLabel(x)
        rxLabel = self.lmpConfigToLabel(y)
        
        for txIdx, rxIdx in zip(txLabel, rxLabel):
            meanMetric = z[idx]
            metricArr[txIdx][rxIdx] = meanMetric
            # print(metricArr[txIdx][rxIdx])
            idx = idx + 1
            
        # round the decimal digits
        return np.round(metricArr, decimals=2)

    def maptoMetricArr(self, txLMPIdxFiles, rxLMPIdxFiles, metricData, meanFlag=True):

        metricArr = np.zeros((5,5))
        x, y, z = 5, 5, 1;
        metricList = [[[] for j in range(y)] for i in range(x)]
        # metricArr = deque([deque()])
        txIdxLMPConfig, rxIdxLMPConfig = deque([]), deque([])
        for fileTxLmps, fileRxLmps in zip(txLMPIdxFiles,rxLMPIdxFiles):
            txIdxLMPConfig.append(self.lmpConfigToLabel(fileTxLmps))
            rxIdxLMPConfig.append(self.lmpConfigToLabel(fileRxLmps))

        # print(txIdxLMPConfig)
        # print(rxIdxLMPConfig)
        # # print(np.shape(metricData))
        # print(metricData[1][6])
        # print(np.mean(metricData[1][6]))
        # print(np.median(metricData[1][6]))
        fIdx, expIdx = 0, 0

        for txIdxArr, rxIdxArr in zip(txIdxLMPConfig, rxIdxLMPConfig):            
            for txIdx, rxIdx in zip(txIdxArr, rxIdxArr):
                metricList[txIdx][rxIdx].extend(metricData[fIdx][expIdx])
                expIdx = expIdx + 1

            fIdx = fIdx + 1
            expIdx = 0
        
        for txIdx in range(np.shape(metricList)[0]):
            for rxIdx in range(np.shape(metricList)[1]):
                if meanFlag:
                    metricArr[txIdx][rxIdx] = np.mean(metricList[txIdx][rxIdx])
                else:
                    metricArr[txIdx][rxIdx] = np.median(metricList[txIdx][rxIdx])

        return np.asarray(metricArr)
    
    def slotspersecToKBph(self, thSlotspersec):
        thBps = np.multiply(np.multiply(thSlotspersec, 4), 3600)
        thkBph = np.divide(thBps, 1024)
        return thkBph     

    def plotSenderStatvsLifecycle(self, onTimesTx, onTimesNumPktsTx, onTimesNumAcks, numOfLifeCycletoPlot=0, printToPdf=False):
        onTimesTxNum = np.arange(0, np.size(onTimesTx), 1)
        if numOfLifeCycletoPlot > 0:
            onTimesTxNum = onTimesTxNum[0:numOfLifeCycletoPlot]
            onTimesTx = onTimesTx[0:numOfLifeCycletoPlot]
            onTimesNumPktsTx = onTimesNumPktsTx[0:numOfLifeCycletoPlot]
            onTimesNumAcks = onTimesNumAcks[0:numOfLifeCycletoPlot]
        
        # Convert on-time to ms
        # onTimesTx = np.multiply(onTimesTx, 1000)

        params = {'figure.figsize': [15, 6],
                'legend.fontsize': 15}
        plt.rcParams.update(params)

        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.title(f"Sender Stats", fontsize=20)
        plt.xlabel('Lifecycle sequence #', size=20)
        plt.ylabel('#', size=20)

        width = 0.25    
        plt.bar(onTimesTxNum, onTimesTx,  width, align='edge', color=self.onTimesTxColor, label='On-time (ms)')
        plt.bar(onTimesTxNum + 2*width, onTimesNumPktsTx, width, align='edge', color=self.pktsTxColor, label='# tx')
        plt.bar(onTimesTxNum + 3*width, onTimesNumAcks, width, align='edge', color=self.ackRxColor, label='# ack')

        plt.yscale("log")
        # plt.xlim(50, 70)

        plt.legend(loc='best')
        plt.tight_layout()
        if printToPdf:
            plt.savefig(f'sender_stats.pdf', dpi=500, format='pdf')
        plt.show()

    def plotReceiverStatvsLifecycle(self, onTimesRx, onTimesNumPktsRx, numOfLifeCycletoPlot, printToPdf=False):
        onTimesRxNum = np.arange(0, np.size(onTimesRx), 1)
        
        if numOfLifeCycletoPlot > 0:
            onTimesRxNum = onTimesRxNum[0:numOfLifeCycletoPlot]
            onTimesRx = onTimesRx[0:numOfLifeCycletoPlot]
            onTimesNumPktsRx = onTimesNumPktsRx[0:numOfLifeCycletoPlot]

        # Convert on-time to ms
        # onTimesRx = np.multiply(onTimesRx, 1000)

        params = {'figure.figsize': [15, 6], 'legend.fontsize': 15}
        plt.rcParams.update(params)

        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.title(f"Receiver Stats", fontsize=20)
        plt.xlabel('Lifecycle sequence #', size=20)
        plt.ylabel('#', size=20)

        width = 0.5       
        plt.bar(onTimesRxNum, onTimesRx , width, align='edge', color=self.onTimeRxColor, label='On-time (ms)')
        plt.bar(onTimesRxNum + width, onTimesNumPktsRx, width, align='edge', color=self.pktRxColor, label='# Rx')

        plt.yscale("log")
        # plt.xlim(0, 100)

        plt.legend(loc='best')
        plt.tight_layout()
        if printToPdf:
            plt.savefig(f'receiver_stats.pdf', dpi=500, format='pdf')
        plt.show()

    
    def plotStatsVsTime(self, txRxData, startTime=0, endTime=1, printToPdf=False):
        # txRxData = [totalTime, onoffTimeTx, numPktsTransmitted, numAcks, pktsRx, onoffTimeRx]
        pktTxTS = txRxData[np.where(txRxData[:,2]>1), 0][0]
        pktTx = txRxData[np.where(txRxData[:,2]>1), 2][0]
        onTimeTx = txRxData[np.where(txRxData[:,2]>1), 1][0]*(10**3)


        # pktAckTS = txRxData[np.where(txRxData[:,2]>0), 0][0]*(10**3)
        pktAck = txRxData[np.where(txRxData[:,2]>1), 3][0]

        pktRxTS = txRxData[np.where(txRxData[:,4]>1), 0][0]
        pktRx = txRxData[np.where(txRxData[:,4]>1), 4][0]

        rxDataTS = txRxData[np.where(txRxData[:,5]>0), 0][0]
        onOffTimeDataRx = txRxData[np.where(txRxData[:,5]>0), 5][0]

        if onOffTimeDataRx[0] > onOffTimeDataRx[1]:
            onTimesDataRx = onOffTimeDataRx[1::2]
            onTimesDataTS = rxDataTS[1::2]
            # offTimesDataRx = onOffTimeDataRx[::2]
            # offTimesDataTS = rxDataTS[::2]
        else:
            onTimesDataRx = onOffTimeDataRx[::2]
            onTimesDataTS = rxDataTS[::2]
            # offTimesDataRx = onOffTimeDataRx[1::2]
            # offTimesDataTS = rxDataTS[1::2]

        onTimesDataRx = np.multiply(onTimesDataRx, 1000)

        params = {'figure.figsize': [15, 6],
                'legend.fontsize': 15}
        plt.rcParams.update(params)

        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        # plt.title(f"Sender Stats", fontsize=20)
        plt.xlabel('Time (s)', size=20)
        plt.ylabel('#', size=20)

        plt.scatter(pktTxTS, onTimeTx, marker='+', s=100, color=self.onTimesTxColor, label="Sender's On-time (ms)")        
        plt.scatter(onTimesDataTS, onTimesDataRx, marker='*', s=100, color="seagreen", label="Receiver's On-time (ms)")     
        plt.scatter(pktTxTS, pktTx, marker='^', s=100, color=self.pktsTxColor, label='# tx')
        plt.scatter(pktTxTS, pktAck, marker='s', s=70, color=self.ackRxColor, label='# acks')
        plt.scatter(pktRxTS, pktRx, marker='o', s=50, color=self.pktRxColor, label='# rx')

        plt.yscale("log")
        plt.xlim(startTime, endTime)

        plt.legend(loc='best')
        plt.tight_layout()
        if printToPdf:
            plt.savefig(f'statsvstimes.pdf', dpi=500, format='pdf')
        plt.show()

    def plotBW(self, avgBytesTxBW, avgBytesRxBW, avgBytesAckBW, expNo, bytesPerHour=True, printToPdf=False):
        params = {'figure.figsize': [6, 5],
        'legend.fontsize': 15}
        plt.rcParams.update(params)

        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.title(f"Throughput {expNo}", fontsize=20)
        if bytesPerHour:
            plt.ylabel('Avg Bytes/hour', size=20)
        else:
            plt.ylabel('Avg Bytes/min', size=20)

        width = 0.5 
        xlabels = ['Tx', 'Rx', 'Ack'] 
        ylables = [np.round(avgBytesTxBW, decimals=3), np.round(avgBytesRxBW, decimals=3), np.round(avgBytesAckBW, decimals=3)]        
        colors = [self.pktsTxColor, self.pktRxColor, self.ackRxColor]
        plt.text(x=0.6,y=avgBytesTxBW-(0.1*avgBytesTxBW), s=f'Number of Bytes in Packet = {self.bytesPerPkt}', fontsize=12, bbox=dict(facecolor='none', edgecolor='black', boxstyle='round,pad=1'))
        for idx, y in enumerate(ylables):
            plt.text(idx-0.2, y+1000, str(y), fontsize=12)
        plt.bar(xlabels, ylables, width, align='center', color=colors)
        plt.tight_layout()
        if printToPdf:
            plt.savefig(f'avg_bw_bytesHour_{bytesPerHour}_bytesperpkt_{self.bytesPerPkt}.pdf', dpi=500, format='pdf')
        plt.show()

    # *broken
    def plotSuccTx(self, succTxPercent, avgTxLifeCycle, avgRxLifeCycle, printToPdf=False):
        succTxPercent = np.asarray(succTxPercent)
        avgTxLifeCycle = np.asarray(avgTxLifeCycle)
        avgRxLifeCycle = np.asarray(avgRxLifeCycle)

        txLifeCycleLmps = []
        rxLifeCycleLmps = []
        succTxLmps = []
        for idx, _ in enumerate(avgTxLifeCycle):
            txLifeCycleLmps.append(np.round(avgTxLifeCycle[idx], decimals=2))
            rxLifeCycleLmps.append(np.round(avgRxLifeCycle[idx], decimals=2))
            succTxLmps.append(succTxPercent[idx, :])

        params = {'figure.figsize': [10, 8],
                'font.family': "Times New Roman",
                'legend.fontsize': 15}
        plt.rcParams.update(params)

        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)

        xlabels = ['LMP-1', 'LMP-5', 'LMP-10', 'LMP-15', 'NULL-LMP']
        x = np.arange(succTxPercent[0, :].shape[0])  # the label locations
        width = 0.2  # the width of the bars

        plt.bar(x-0.2, succTxLmps[0], width, label=f'txLF:{txLifeCycleLmps[0]}, rxLF:{rxLifeCycleLmps[0]}')
        plt.bar(x, succTxLmps[1], width, label=f'txLF:{txLifeCycleLmps[1]}, rxLF:{rxLifeCycleLmps[1]}')
        plt.bar(x+0.2, succTxLmps[2], width, label=f'txLF:{txLifeCycleLmps[2]}, rxLF:{rxLifeCycleLmps[2]}')
        plt.bar(x+0.4, succTxLmps[3], width, label=f'txLF:{txLifeCycleLmps[3]}, rxLF:{rxLifeCycleLmps[3]}')

        plt.ylabel('Successful Tx %', fontsize=18)
        plt.xticks(x, xlabels, fontsize=18)

        plt.legend(loc='best')

        if printToPdf:
            plt.savefig(f'succ_rate.pdf', dpi=500, format='pdf')

        plt.tight_layout()

    def plotAggStats(self, succPercent, avgTxLfc, avgRxLfc, avgIntArrivalTime, throughput, onTimesTxExp, onTimesRxExp, medOnTimesTxExp, medOnTimesRxExp, offTimesTxExp, offTimesRxExp, overlapPercentExp, noTxOnTimesPercentExp, numPktsTxExp,  medPktsAckSuccOntimeExp, meanPktsAckSuccOntimeExp, numSuccOnTimeExp, expNum, senderLMPs, receiverLMPs, date, printToPdf=False):
        succTxPercent = np.asarray(succPercent)
        avgTxLifeCycle = np.asarray(avgTxLfc)
        avgRxLifeCycle = np.asarray(avgRxLfc)
        throughput = np.divide(throughput, 1000)
        numPktsTxExp = np.divide(numPktsTxExp, 1000)

        numOfSuccTxPh = throughput[:, 2]
        nullLMPTxAttempt = throughput[4, 0]
        normSuccTxPercent = np.divide(numOfSuccTxPh, nullLMPTxAttempt)*100

        fig, ax1 = plt.subplots()
        fig.fontname = "Times New Roman"
        fig.set_size_inches(20, 8, forward=True)

        groupBarsDist = 7
        x = np.arange(0, len(succTxPercent)*groupBarsDist, groupBarsDist)  # the label locations
        # xlabels = ['LMP-1', 'LMP-5', 'LMP-10', 'LMP-15', 'NULL-LMP']
        xlabels, rxLMPConfig = self.findLMPConfigExp(medOnTimesTxExp, medOnTimesRxExp)

        colorsAx1 = ['dimgray', 'teal', 'darkorange', 'saddlebrown', 'yellowgreen',  'goldenrod', 'royalblue', 'purple',  'red', 'darkgrey', 'lightpink', 'lightblue']
        colorsAx2 = ['magenta']

        ax1.set_title(f'Date: {date}', fontsize=20)

        # x = np.arange(len(succTxPercent))
        width = 0.4  # the width of the bars"

        decimalPoints = 2
        ylablesAx1 = [np.round(throughput[:,2], decimals=decimalPoints), np.round(avgIntArrivalTime[:, 0], decimals=decimalPoints), np.round(normSuccTxPercent, decimals=decimalPoints), np.round(avgTxLifeCycle, decimals=decimalPoints), np.round(avgRxLifeCycle, decimals=decimalPoints), np.round(overlapPercentExp, decimals=decimalPoints), np.round(numPktsTxExp, decimals=decimalPoints), np.round(medPktsAckSuccOntimeExp, decimals=decimalPoints), np.round(numSuccOnTimeExp, decimals=decimalPoints), np.round(meanPktsAckSuccOntimeExp, decimals=decimalPoints), np.round(medOnTimesTxExp, decimalPoints), np.round(medOnTimesRxExp, decimalPoints)]
        barLabelsAx1 = [f'Throughput (kB/hr)', f'Avg Inter-handshake interval (s)', f'Normalized succ Tx %', f'Avg Tx lifecycle', f'Avg Rx lifecycle', f'Tx-Rx Overlap %', f'Num of Tx (K)', f'Median succ Tx per succ on-time', f'# succ on-time', f'Mean succ Tx per succ on-time', f'Median on-time tx', f'Median on-time rx']
        ylablesAx2 = [np.round(noTxOnTimesPercentExp, decimals=decimalPoints)]
        barLabelsAx2 = [f'No tx on-times %']

        ax1.set_xlabel(f"Sender's LMP config {senderLMPs}, Receiver's LMP config [{rxLMPConfig}]", fontsize=20)
        ax1.set_ylabel('#', color="black", fontsize=20)
        ax1.set_xticks(x)
        ax1.tick_params(axis='y', labelcolor="black", labelsize=20)
        ax1.set_xticklabels(xlabels, fontsize=20)

        ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

        numBarsAx1 = len(ylablesAx1)
        # numBarsAx2 = len(ylablesAx2)

        barsPosAx1 = np.subtract(np.arange(0, numBarsAx1*width, width), (numBarsAx1*width)/2)
        # barsPosAx2 = np.subtract(np.arange(barsPosAx1[-1], numBarsAx2/10, width), numBarsAx2/20)

        for bar in range(numBarsAx1):
            rects1 = ax1.bar(x-barsPosAx1[bar], ylablesAx1[bar], width, color=colorsAx1[bar], label=barLabelsAx1[bar])
            ax1.bar_label(rects1, padding=3)

        # for bar in range(numBarsAx2):
        #     rects2 = ax2.bar(x-barsPosAx2[bar], ylablesAx2[bar], width, color=colorsAx2[bar], label=barLabelsAx2[bar])
        #     ax2.bar_label(rects2, padding=3)
        rects2 = ax2.bar(x+barsPosAx1[-1]+(2*width), ylablesAx2[0], width, color=colorsAx2[0], label=barLabelsAx2[0])
        ax2.bar_label(rects2, padding=3)

        ax2.set_ylabel('No tx on-time %', color=colorsAx2[0], fontsize=20)

        ax1.set_yscale('log')
        ax1.set_ylim([1, 10000])
        ax2.set_ylim([None, 10], auto=True)
        ax2.set_yscale('log')

        # ax1.legend(loc='upper left', ncol=4, fontsize=10)
        # ax2.legend(loc='center right', fontsize=10)
        fig.legend(loc='upper right', bbox_to_anchor=(0.45, 0., 0.5, 0.95), ncol=4, fontsize=15)

        fig.tight_layout()
        if printToPdf:
            fig.savefig(f'agg_stats_tx_{senderLMPs}_rx_{rxLMPConfig}_{date}.pdf', dpi=500, format='pdf')

        plt.show()

    def plotExpDivAggStats(self, succPercent, avgTxLfc, avgRxLfc, avgIntArrivalTime, throughput, onTimesTxExp, onTimesRxExp, offTimesTxExp, offTimesRxExp, overlapPercentExp, noTxOnTimesPercentExp, numPktsTxExp,  medPktsAckSuccOntimeExp, numSuccOnTimeExp, expNum, senderLMPs, receiverLMPs, expDivKey, lmpConfig, date, printToPdf=False):
        succTxPercent = np.asarray(succPercent)
        avgTxLifeCycle = np.asarray(avgTxLfc)
        avgRxLifeCycle = np.asarray(avgRxLfc)
        # throughput = np.divide(throughput, 1000)
        numPktsTxExp = np.divide(numPktsTxExp, 1000)

        numOfSuccTxPh = throughput[:, 2]
        nullTxAttempt = throughput[:, 1]
        normSuccTxPercent = np.divide(numOfSuccTxPh, nullTxAttempt)*100

        fig, ax1 = plt.subplots()
        fig.fontname = "Times New Roman"
        fig.set_size_inches(18, 8, forward=True)

        groupBarsDist = 5
        x = np.arange(0, len(succTxPercent)*groupBarsDist, groupBarsDist)  # the label locations
        xlabels = np.repeat(lmpConfig, 4)

        colorsAx1 = ['dimgray', 'teal', 'darkorange', 'saddlebrown', 'yellowgreen',  'goldenrod', 'royalblue', 'purple', 'red']
        colorsAx2 = ['magenta']

        # x = np.arange(len(succTxPercent))
        width = 0.4  # the width of the bars"

        decimalPoints = 2
        ylablesAx1 = [np.round(throughput[:,2], decimals=decimalPoints), np.round(avgIntArrivalTime[:, 0], decimals=decimalPoints), np.round(normSuccTxPercent, decimals=decimalPoints), np.round(avgTxLifeCycle, decimals=decimalPoints), np.round(avgRxLifeCycle, decimals=decimalPoints), np.round(overlapPercentExp, decimals=decimalPoints), np.round(numPktsTxExp, decimals=decimalPoints), np.round(medPktsAckSuccOntimeExp, decimals=decimalPoints), np.round(numSuccOnTimeExp, decimals=decimalPoints)]
        barLabelsAx1 = [f'Throughput (Bytes/min)', f'Avg Inter-handshake interval (s)', f'Succ Tx %', f'Avg Tx lifecycle', f'Avg Rx lifecycle', f'Tx-Rx Overlap %', f'Num of Tx', f'Median succ Tx per succ on-time', f'# succ on-time']
        ylablesAx2 = [np.round(noTxOnTimesPercentExp, decimals=decimalPoints)]
        barLabelsAx2 = [f'No tx on-times %']

        ax1.set_xlabel(f"Sender's LMP config {senderLMPs}, Receiver's LMP config [{receiverLMPs}]", fontsize=20)
        ax1.set_ylabel('#', color="black", fontsize=20)
        ax1.set_xticks(x)
        ax1.tick_params(axis='y', labelcolor="black", labelsize=20)
        ax1.set_xticklabels(xlabels, fontsize=20)

        ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

        numBarsAx1 = len(ylablesAx1)
        # numBarsAx2 = len(ylablesAx2)

        barsPosAx1 = np.subtract(np.arange(0, numBarsAx1*width, width), (numBarsAx1*width)/2)
        # barsPosAx2 = np.subtract(np.arange(barsPosAx1[-1], numBarsAx2/10, width), numBarsAx2/20)

        for bar in range(numBarsAx1):
            rects1 = ax1.bar(x-barsPosAx1[bar], ylablesAx1[bar], width, color=colorsAx1[bar], label=barLabelsAx1[bar])
            ax1.bar_label(rects1, padding=3)

        # for bar in range(numBarsAx2):
        #     rects2 = ax2.bar(x-barsPosAx2[bar], ylablesAx2[bar], width, color=colorsAx2[bar], label=barLabelsAx2[bar])
        #     ax2.bar_label(rects2, padding=3)
        rects2 = ax2.bar(x+barsPosAx1[-1]+(2*width), ylablesAx2[0], width, color=colorsAx2[0], label=barLabelsAx2[0])
        ax2.bar_label(rects2, padding=3)

        ax2.set_ylabel('No tx on-time %', color=colorsAx2[0], fontsize=20)

        ax1.set_yscale('log')
        ax1.set_ylim([1, 10000])
        ax2.set_ylim([0.01, 10])
        ax2.set_yscale('log')

        # ax1.legend(loc='upper left', ncol=4, fontsize=10)
        # ax2.legend(loc='center right', fontsize=10)
        fig.legend(loc='upper right', bbox_to_anchor=(0.4, 0., 0.5, 0.98), ncol=4, fontsize=15)

        fig.tight_layout()
        if printToPdf:
            fig.savefig(f'agg_stats_tx_{senderLMPs}_rx_{receiverLMPs}_{lmpConfig}_{expDivKey}_{date}.pdf', dpi=500, format='pdf')

        plt.show()

    def plot3DScatter(self, x, y, z, avgTxLifeCycle, avgRxLifeCycle, metric, expDate, printToPdf=False):
        # Creating figure
        fig = plt.figure(figsize = (10, 7))
        ax = plt.axes(projection ="3d")
        
        # Add x, y gridlines
        ax.grid(b = True, color ='grey', linestyle ='-.', linewidth = 0.3, alpha = 0.2)    
        
        if metric == 'throughput':
            plottitle = 'Throughput (kB/hr)'
            customCmap = 'viridis'
        elif metric == 'inter-handshake':
            plottitle = 'Inter-handshake interval (s)'
            customCmap = 'viridis_r'
        else:
            plottitle = 'Metric not defined'
        
        # Creating color map
        my_cmap = plt.get_cmap(customCmap)
        
        # Creating plot
        sctt = ax.scatter3D(x, y, z, alpha = 0.8, c = z, cmap = my_cmap, marker ='^')
        # ax.plot_surface(x, y, z, cmap=my_cmap)

        plt.title(f'{plottitle}, Avg tx lifecycle: {avgTxLifeCycle}, Avg rx lifecycle: {avgRxLifeCycle}', fontsize=10)
        ax.set_xlabel('LMP-config tx')
        ax.set_ylabel('LMP-config rx')

        cax = fig.add_axes([ax.get_position().x1+0.02,ax.get_position().y0,0.02,ax.get_position().height])
        # fig.colorbar(sctt, ax = ax, shrink = 0.5, aspect = 5)
        fig.colorbar(sctt, cax=cax)
  

        if printToPdf:
            fig.savefig(f'sctt3D_tx_rx_{metric}_{expDate}_lfc_tx_{avgTxLifeCycle}_rx_{avgRxLifeCycle}.pdf', dpi=500, format='pdf')
        
        # show plot
        plt.show()
        
    def plot3DSurface(self, x, y, z, avgTxLifeCycle, avgRxLifeCycle, metric, expDate, printToPdf=False):
        # Creating figure
        # fig = plt.figure(figsize = (10, 7))
        fig = plt.figure(figsize = (5, 5))
        # ax = plt.axes(projection ="3d")
        # ax = Axes3D(fig, auto_add_to_figure=False, title=f'Avg tx lifecycle: {avgTxLifeCycle}, Avg rx lifecycle: {avgRxLifeCycle}')
        ax = plt.axes(projection ="3d")

        z = np.asarray(z)
        z = z.flatten()  

        # check for Nan values
        if len(np.where(np.isnan(z))[0]):
            x = np.delete(x, np.argwhere(np.isnan(z))[0])
            y = np.delete(y, np.argwhere(np.isnan(z))[0])
            z = np.delete(z, np.argwhere(np.isnan(z))[0])

        # check for zero elements
        # print(z)
        # print(np.where(z == 0)[0])
        
        # Add x, y gridlines
        ax.grid(b = True, color ='grey', linestyle ='-.', linewidth = 0.3, alpha = 0.2)     
        
        if metric == 'throughput':
            plottitle = 'Throughput (kB/hr)'
            customCmap = 'viridis'
        elif metric == 'inter-handshake':
            plottitle = 'TTH (s)'
            customCmap = 'viridis_r'
        else:
            plottitle = 'Metric not defined'

        # print(f'Avg tx lifecycle: {avgTxLifeCycle}, Avg rx lifecycle: {avgRxLifeCycle}')
            
        # ax.set_title(f'{plottitle}', fontsize = 20)
        ax.set_title(f'{plottitle}', y=1.05, pad=2, fontsize=20)
        ax.set_xlabel('LMP-config tx', fontsize=12)
        ax.set_ylabel('LMP-config rx', fontsize=12)
        ax.tick_params(axis='x', labelsize=10)
        ax.tick_params(axis='y', labelsize=10)
        ax.tick_params(axis='z', labelsize=10)
        
        # Creating color map
        my_cmap = plt.get_cmap(customCmap)

        surf = ax.plot_trisurf(x, y, z, cmap=my_cmap, linewidth=0.1)
        
        # Creating plot
        # sctt = ax.scatter3D(x, y, z, alpha = 0.8, c = z, cmap = my_cmap, marker ='^')
        if expDate == '04232021' and metric == 'inter-handshake':
            ax.set_zlim(0, 20)

        cax = fig.add_axes([ax.get_position().x1+0.03,ax.get_position().y0,0.03,ax.get_position().height])
        # fig.colorbar(sctt, ax = ax, shrink = 0.5, aspect = 5)
        fig.colorbar(surf, cax=cax)
        
        ax.view_init(elev=self.elev, azim=self.azim)
        # bottom=bottom_pos, top=top_pos, left=left_pos, right=right_pos
        # bbox = fig.bbox_inches.from_bounds(0, 5, 5, 5)
        # fig.tight_layout()
        if printToPdf:
            fig.savefig(f'surface3D_tx_rx_{metric}_{expDate}_lfc_tx_{avgTxLifeCycle}_rx_{avgRxLifeCycle}.pdf', bbox_inches='tight', dpi=500, format='pdf')
            # fig.savefig(f'surface3D_tx_rx_{metric}_{expDate}.pdf', dpi=500, format='pdf')
        
        # show plot
        plt.show()
        
    def plotHeatMap(self, x, y, z, avgTxLifeCycle, avgRxLifeCycle, metric, expDate, printToPdf=False):
        txLMPConfigs = ["LMP-1", "LMP-5", "LMP-10", "LMP-15",
              "LMP-Null"]
        rxLMPConfigs = ["LMP-1", "LMP-5", "LMP-10", "LMP-15",
              "LMP-Null"]
        idx = 0
        metricArr = np.zeros((5, 5))
        
        txLabel = self.lmpConfigToLabel(x)
        rxLabel = self.lmpConfigToLabel(y)
        
        for txIdx, rxIdx in zip(txLabel, rxLabel):
            metricArr[txIdx][rxIdx] = z[idx]
            idx = idx + 1
            
        # round the decimal digits
        metricArr = np.round(metricArr, decimals=2)
        # Remove zeros from array
        metricArrMasked = np.ma.masked_where(metricArr == 0, metricArr)
        
        if metric == 'throughput':
            plottitle = 'Throughput (kB/hr)'
            customCmap = 'viridis'
        elif metric == 'inter-handshake':
            plottitle = 'TTH (s)'
            customCmap = 'viridis_r'
        elif metric == 'lifecycle_tx':
            plottitle = 'Lifecycle tx %'
            customCmap = 'viridis'
        elif metric == 'lifecycle_rx':
            plottitle = 'Lifecycle rx %'
            customCmap = 'viridis'
        elif metric == 'model-th':
            plottitle = 'Throughput (kSlots/hr)'
            customCmap = 'viridis'
        elif metric == 'model-tto':
            plottitle = 'TTO (s)'
            customCmap = 'viridis_r'
        else:    
            plottitle = 'Metric not defined'

        # print(z)
        
        fig, ax = plt.subplots(figsize = (10, 7))
        im = ax.imshow(metricArrMasked, cmap=customCmap)
        
        plt.gca().invert_yaxis()
        plt.colorbar(im)            
        

        # We want to show all ticks...
        ax.set_xticks(np.arange(len(txLMPConfigs)))
        ax.set_yticks(np.arange(len(rxLMPConfigs)))
        # ... and label them with the respective list entries
        ax.set_xticklabels(txLMPConfigs)
        ax.set_yticklabels(rxLMPConfigs)
        ax.set_xlabel("Rx LMP Config", fontsize=15)
        ax.set_ylabel("Tx LMP Config", fontsize=15)
        
        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor", fontsize=20)
        plt.setp(ax.get_yticklabels(), ha="right",
                 rotation_mode="anchor", fontsize=20)
        
        # Loop over data dimensions and create text annotations.
        for i in range(len(rxLMPConfigs)):
            for j in range(len(txLMPConfigs)):
                text = ax.text(j, i, metricArrMasked[i, j],
                               ha="center", va="center", color="w")
                
        ax.set_title(f'{plottitle}, Avg tx lifecycle: {avgTxLifeCycle}, Avg rx lifecycle: {avgRxLifeCycle}', fontsize=10)
        fig.tight_layout()
        
        if printToPdf:
            fig.savefig(f'heatmap_tx_rx_{metric}_{expDate}_lfc_tx_{avgTxLifeCycle}_rx_{avgRxLifeCycle}.pdf', dpi=500, format='pdf')
        
        plt.show()
        
    def plot2Dmetric(self, txLMPIdxExp, rxLMPIdxExp, metricData, ontimes, TTOs, throughputs, avgTxLifeCycle, avgRxLifeCycle, metric, model, expDate, printToPdf=False):
        idx = 0
        metricArr = np.zeros((5, 5))
        
        txLabel = self.lmpConfigToLabel(txLMPIdxExp)
        rxLabel = self.lmpConfigToLabel(rxLMPIdxExp)
        
        for txIdx, rxIdx in zip(txLabel, rxLabel):
            metricArr[txIdx][rxIdx] = metricData[idx]
            idx = idx + 1
            
        # round the decimal digits
        metricArr = np.round(metricArr, decimals=2)
        
        lmpConfigs = [1, 5, 10, 15, 45]
        ontimeExp = np.add(np.multiply(lmpConfigs, 5), model.boottime)
        
        TTOsToPlot, throughputsToPlot = [], []        
        for lmp in lmpConfigs:
            TTOsToPlot.append(TTOs[lmp-1])
            throughputsToPlot.append(throughputs[lmp-1])
            
        # Convert from slots/sec to kB/hr
        throughputsToPlot = self.slotspersecToKBph(throughputsToPlot)


        if self.scaling:
            TTOsToPlot = np.multiply(TTOsToPlot, 2)
            throughputsToPlot = np.divide(throughputsToPlot, 2)
        
        # print(throughputsToPlot)

        fig, ax1 = plt.subplots()
        fig.fontname = "Times New Roman"
        fig.set_size_inches(10, 7, forward=True)
        
        if metric == 'throughput':
            plottitle = 'Throughput (kB/hr)'
        elif metric == 'inter-handshake':
            plottitle = 'Inter-handshake interval (s)'
        else:
            plottitle = 'Metric not defined'
            
        xlabels = ['LMP-1', 'LMP-5', 'LMP-10', 'LMP-15', 'LMP-Null']
        explabels = ['LMP-1 exp', 'LMP-5 exp', 'LMP-10 exp', 'LMP-15 exp', 'LMP-Null exp']
        lmpMarkers = ['^', '+', '*', 'D', '4']
        ax1.set_xticks(ontimeExp)
        ax1.set_xticklabels(xlabels, fontsize=20)    
        ax1.tick_params(axis='y', labelsize=15)
        ax1.tick_params(axis='x', labelsize=15, rotation=45)
        ax1.set_ylabel(f'{plottitle}', fontsize=20)
        ax1.set_xlabel('Rx LMP config', fontsize=20)
        ax1.set_title(f'Avg tx lifecycle: {avgTxLifeCycle}, Avg rx lifecycle: {avgRxLifeCycle}', fontsize=10)
        
        for idx in range(np.shape(metricArr)[0]):
            ax1.scatter(ontimeExp, metricArr[:, idx], label=explabels[idx], marker=lmpMarkers[idx])
            
        if metric == 'throughput':
            for idx in range(np.shape(throughputsToPlot)[0]):
                ax1.plot(ontimes, throughputsToPlot[idx], label=xlabels[idx])
        elif metric == 'inter-handshake':
            for idx in range(np.shape(TTOsToPlot)[0]):
                ax1.plot(ontimes, TTOsToPlot[idx], label=xlabels[idx])
            
        plt.legend(loc='best', fontsize=15, ncol=2)
        plt.tight_layout()
            
        # ax.set_title(f'{plottitle}', fontsize=20)
        if printToPdf:
            fig.savefig(f'metric2D_tx_rx_{metric}_{expDate}.pdf', dpi=500, format='pdf')
        
        # show plot
        plt.show()
        
    def plot2DAppendMetric(self, txLMPIdxExp, rxLMPIdxExp, metricData, ontimes, TTOs, throughputs, avgTxLifeCycle, avgRxLifeCycle, metric, model, expDate, printToPdf=False):
       
        lmpConfigs = [1, 5, 10, 15, 42]
        ontimeExp = np.add(np.multiply(lmpConfigs, 5), 15)

        ontimeExpNan = np.delete(ontimeExp, 3)
        metricDataNan = np.delete(metricData[0], 3)
        # print(ontimeExpNan)
        # print(metricDataNan)
        # print(np.argwhere(np.isnan(metricData))[0])

        # ontimeExp = np.delete(ontimeExp, np.argwhere(np.isnan(metricData))[0])
        # y = np.delete(y, np.argwhere(np.isnan(metricData))[0])

        
        TTOsToPlot, throughputsToPlot = [], []        
        for lmp in lmpConfigs:
            TTOsToPlot.append(TTOs[lmp-1])
            throughputsToPlot.append(throughputs[lmp-1])
            
        # Convert from slots/sec to kB/hr
        throughputsToPlot = self.slotspersecToKBph(throughputsToPlot)
        
        # print(throughputsToPlot)
        
        if self.scaling:
            TTOsToPlot = np.multiply(TTOsToPlot, 2)
            throughputsToPlot = np.divide(throughputsToPlot, 2)

        fig, ax1 = plt.subplots()
        fig.fontname = "Times New Roman"
        fig.set_size_inches(10, 7, forward=True)
        
        if metric == 'throughput':
            plottitle = 'Throughput (kB/hr)'
        elif metric == 'inter-handshake':
            plottitle = 'Inter-handshake interval (s)'
        else:
            plottitle = 'Metric not defined'
            
        xlabels = ['LMP-1', 'LMP-5', 'LMP-10', 'LMP-15', 'LMP-Null']
        explabels = ['LMP-1 exp', 'LMP-5 exp', 'LMP-10 exp', 'LMP-15 exp', 'LMP-Null exp']
        lmpMarkers = ['^', '+', '*', 'D', '4']
        ax1.set_xticks(ontimeExp)
        ax1.set_xticklabels(xlabels, fontsize=20)    
        ax1.tick_params(axis='y', labelsize=15)
        ax1.tick_params(axis='x', labelsize=15, rotation=45)
        ax1.set_ylabel(f'{plottitle}', fontsize=20)
        ax1.set_xlabel('Rx LMP config', fontsize=20)
        ax1.set_title(f'Avg tx lifecycle: {avgTxLifeCycle}, Avg rx lifecycle: {avgRxLifeCycle}', fontsize=10)
        
        ax1.scatter(ontimeExpNan, metricDataNan, label=explabels[0], marker=lmpMarkers[0])
        for idx in range(1, np.shape(metricData)[0]):
            print(metricData[idx, :])
            ax1.scatter(ontimeExp, metricData[idx, :], label=explabels[idx], marker=lmpMarkers[idx])
            
        if metric == 'throughput':
            for idx in range(np.shape(throughputsToPlot)[0]):
                ax1.plot(ontimes, throughputsToPlot[idx], label=xlabels[idx])
        elif metric == 'inter-handshake':
            for idx in range(np.shape(TTOsToPlot)[0]):
                ax1.plot(ontimes, TTOsToPlot[idx], label=xlabels[idx])
            
        plt.legend(loc='upper left', fontsize=10, ncol=2)
        plt.tight_layout()
            
        # ax.set_title(f'{plottitle}', fontsize=20)
        if printToPdf:
            fig.savefig(f'metric2D_tx_rx_{metric}_{expDate}.pdf', dpi=500, format='pdf')
        
        # show plot
        plt.show()
        

    def plotModelMetrics(self, ontimes, TTOs, throughputs, avgTxLifeCycle, avgRxLifeCycle, use3D=True):
        if use3D:
            X = np.divide(ontimes - self.model.boottime, self.model.s)
            Y = np.divide(ontimes - self.model.boottime, self.model.s)
            X, Y = np.meshgrid(X, Y)
            
        if use3D:
            fig = plt.figure(0)
            ax = fig.add_subplot(projection='3d')
            surface = ax.plot_surface(X, Y, TTOs, cmap='viridis_r')
            plt.colorbar(surface)

            fig = plt.figure(1)
            ax = fig.add_subplot(projection='3d')
            surface = ax.plot_surface(X, Y, throughputs, cmap='viridis')
            plt.colorbar(surface)

        else:
            plt.figure(0)
            plt.contour(np.divide(ontimes - self.model.boottime, self.model.s), np.divide(ontimes - self.model.boottime, self.model.s), TTOs, 40, cmap='viridis_r')
            plt.colorbar()

            plt.figure(1)
            plt.contour(np.divide(ontimes - self.model.boottime, self.model.s), np.divide(ontimes - self.model.boottime, self.model.s), throughputs, 40, cmap='viridis')
            plt.colorbar()

        plt.figure(0)
        plt.xlabel('On-time of node A (slots)')
        plt.ylabel('On-time of node B (slots)')
        plt.title(f'Expected Time-to-Overlap (s), Avg tx lifecycle: {avgTxLifeCycle}, Avg rx lifecycle: {avgRxLifeCycle}')
        plt.savefig('lmp_tto_heatmap.pdf')
        plt.show()

        plt.figure(1)
        plt.xlabel('On-time of node A (slots)')
        plt.ylabel('On-time of node B (slots)')
        plt.title('Expected Throughput (slots/sec)')
        plt.savefig('lmp_throughput_heatmap.pdf')
        plt.show()
        
    def plotBootTimeVar(self, txLMPIdxExp, rxLMPIdxExp, handshake, throughput, avgTxLifeCycle, avgRxLifeCycle, expDate, printToPdf=False):
        fig, ax1 = plt.subplots()
        fig.fontname = "Times New Roman"
        fig.set_size_inches(10, 7, forward=True)
            
        bootTimeIdx = np.arange(0, 5, 1)
        bootTimeLabels = ['20 ms', '40 ms', '60 ms', '80 ms', '100 ms']

        # print(handshake)
        # print(throughput)
        
        ax1.set_xticks(bootTimeIdx)
        ax1.set_xticklabels(bootTimeLabels, fontsize=20)   
        ax1.set_ylabel("Inter-handshake interval (s)", fontsize=20)
        ax1.set_xlabel("Boot-time delay (ms)", fontsize=20)
        ax1.tick_params(axis='y', labelsize=15)

        ax1.plot(bootTimeIdx[0:4], handshake[0:4], label="inter-handshake interval", color='blue')    
        ax1.plot(bootTimeIdx[-1], 0, marker='x', markersize=30, color='red')
        
        ax2 = ax1.twinx()
        
        ax2.set_ylabel("Throughput (kB/hr)", fontsize=20)
        ax2.tick_params(axis='y', labelsize=15)
        ax2.plot(bootTimeIdx[0:4], throughput[0:4], label="throughput", color='red')
        
        
        ax1.legend(loc='best', fontsize=15)
        ax2.legend(loc='best', fontsize=15)
        plt.tight_layout()
        
        if printToPdf:
            fig.savefig(f'boottime_variations_{expDate}.pdf', dpi=500, format='pdf')
        
        # show plot
        plt.show()

    def plotModelvsExpMetrics(self, txLMPIdxExp, rxLMPIdxExp, metricData, ontimes, TTOs, throughputs, model, avgTxLifeCycle, avgRxLifeCycle, metric, expDate, use3D=True, printToPdf=False):
        if use3D:
            X = np.divide(ontimes - model.boottime, model.s)
            Y = np.divide(ontimes - model.boottime, model.s)
            X, Y = np.meshgrid(X, Y)         

        # Creating figure
        fig = plt.figure(figsize = (5, 5))
        ax = plt.axes(projection ="3d")         
        
        # Add x, y gridlines
        ax.grid(b = True, color ='grey', linestyle ='-.', linewidth = 0.3, alpha = 0.2)     
        
        if metric == 'throughput':
            plottitle = 'OPS (kSlots/hr)'
            customCmap = 'viridis'
            if self.scaling:
                Z = np.divide(self.slotspersecToKBph(throughputs), 2)
            else:
                # Z = self.slotspersecToKBph(throughputs)
                Z = np.divide(np.multiply(throughputs, 3600), 1000)
        elif metric == 'inter-handshake':
            plottitle = 'TTO (s)'
            customCmap = 'viridis_r'
            if self.scaling:
                Z = np.multiply(TTOs, 2)
            else:
                Z = TTOs
        else:
            plottitle = 'Metric not defined'

        # print(f'Avg tx lifecycle: {avgTxLifeCycle}, Avg rx lifecycle: {avgRxLifeCycle}') 
        ax.set_title(f'{plottitle}', y=1.05, pad=2, fontsize=20)
        ax.set_xlabel('LMP-config tx', fontsize=12)
        ax.set_ylabel('LMP-config rx', fontsize=12)
        ax.tick_params(axis='x', labelsize=10)
        ax.tick_params(axis='y', labelsize=10)
        ax.tick_params(axis='z', labelsize=10)
        
        # Creating color map
        my_cmap = plt.get_cmap(customCmap)
                
        surf = ax.plot_surface(X, Y, Z, cmap=my_cmap, linewidth=0.1)
        
        # Creating plot
        # sctt = ax.scatter3D(txLMPIdxExp, rxLMPIdxExp, metricData, alpha = 0.8, c = metricData, cmap = my_cmap, marker ='^')

        cax = fig.add_axes([ax.get_position().x1+0.03,ax.get_position().y0,0.03,ax.get_position().height])
        # fig.colorbar(sctt, ax = ax, shrink = 0.5, aspect = 5)
        fig.colorbar(surf, cax=cax)
        ax.view_init(elev=self.elev, azim=self.azim)
        
        if printToPdf:
            fig.savefig(f'surface3D_model_{metric}_{expDate}_lfc_tx_{avgTxLifeCycle}_rx_{avgRxLifeCycle}.pdf', bbox_inches='tight', dpi=500, format='pdf')
        
        # show plot
        plt.show()

    def plotModelvsExpSubplots(self, txLMPIdxExp, rxLMPIdxExp, metricData, ontimes, TTOs, throughputs, model, avgTxLifeCycle, avgRxLifeCycle, metric, expDate, use3D=True, printToPdf=False):
        if use3D:
            X = np.divide(ontimes - model.boottime, model.s)
            Y = np.divide(ontimes - model.boottime, model.s)
            X, Y = np.meshgrid(X, Y)         

        # Creating figure
        fig = plt.figure(figsize = (10, 7))
        # ax = plt.axes(projection ="3d")     
        # fig, (ax1, ax2) = plt.subplots(1, 2)       
        
        if metric == 'throughput':
            plottitle = 'Throughput (kB/hr)'
            customCmap = 'viridis'
            if self.scaling:
                Z = np.divide(self.slotspersecToKBph(throughputs), 2)
            else:
                Z = self.slotspersecToKBph(throughputs)
        elif metric == 'inter-handshake':
            plottitle = 'Inter-handshake interval (s)'
            customCmap = 'viridis_r'
            if self.scaling:
                Z = np.multiply(TTOs, 2)
            else:
                Z = TTOs
        else:
            plottitle = 'Metric not defined'
            
        # ax.set_title(f'{plottitle}, Avg tx lifecycle: {avgTxLifeCycle}, Avg rx lifecycle: {avgRxLifeCycle}', fontsize = 10)
        
        # ax.set_xlabel('LMP-config tx', fontsize = 10)
        # ax.set_ylabel('LMP-config rx', fontsize = 10)
        
        # Creating color map
        my_cmap = plt.get_cmap(customCmap)
        
        # set up the axes for the first plot
        ax1 = fig.add_subplot(1, 2, 1, projection='3d')
        # surf = ax.plot_surface(X, Y, Z, cmap=my_cmap, linewidth=0.1)
        surf1 = ax1.plot_surface(X, Y, Z, cmap=my_cmap, linewidth=0.1)
        ax1.view_init(elev=self.elev, azim=self.azim)

        # fig.suptitle(f'{plottitle}', fontsize = 10)

        ax1.set_xlabel('LMP-config tx', fontsize = 10)
        ax1.set_ylabel('LMP-config rx', fontsize = 10)
        # create subplot
        ax2 = fig.add_subplot(1, 2, 2, projection='3d')
        surf2 = ax2.plot_trisurf(txLMPIdxExp, rxLMPIdxExp, metricData, cmap=my_cmap, linewidth=0.1)        
        ax2.set_xlabel('LMP-config tx', fontsize = 10)
        ax2.set_ylabel('LMP-config rx', fontsize = 10)
        ax2.view_init(elev=self.elev, azim=self.azim)

        cax = fig.add_axes([ax2.get_position().x1+0.02,ax2.get_position().y0,0.02,ax2.get_position().height])
        # fig.colorbar(surf2, ax = ax, shrink = 0.5, aspect = 5)
        fig.colorbar(surf2, cax=cax)
        
        # fig.tight_layout()
        if printToPdf:
            fig.savefig(f'surafce3D_subplot_modelvsexp_{metric}_{expDate}.pdf', dpi=500, format='pdf')
        
        # show plot
        plt.show()


def main():
    import analyzer
    # plotter = analyzer.Plotter()
    ar = analyzer.AnalysisRunner()

    # expFiles = ["../exp_data/04212021/tx_[1,5,10,15,0]_rx_[1,5,10,15,0]   .txt", "../exp_data/04222021/tx_[1,5,10,15,0]_rx_[1,5,10,15,0].txt", "../exp_data/04232021/tx_[1,5,10,15,0]_rx_[1,5,10,15,0].txt", "../exp_data/04242021/tx_[1,5,10,15,0]_rx_[1,5,10,15,0]_lf_5_1.txt"]
    # expDates = ["04212021", "04222021", "04232021", "04242021"]
    # expFiles = ["../exp_data/04212021/tx_[1,5,10,15,0]_rx_[1,5,10,15,0].txt"]
    # expFiles = ["../exp_data/04242021/tx_[1,5,10,15,0]_rx_[1,5,10,15,0]_lf_5_1.txt"]
    # expFiles = ["../exp_data/04232021/tx_[1,5,10,15,0]_rx_[1,5,10,15,0].txt"]
    # expFiles = ["../exp_data/04292021/tx_[1,5,10,15,0]_rx_[1,5,10,15,0]_updated.txt"]
    # expFiles = ["../exp_data/04302021/tx_[1,5,10,15,0]_rx_[1,5,10,15,0]_lf_5_1_updated.txt"]
    # expFiles = ["../exp_data/05062021/tx_10_rx_10_noacks_1.txt", "../exp_data/05062021/tx_10_rx_10_noacks_3.txt", "../exp_data/05062021/tx_10_rx_10_noacks_5.txt"]

    # expFiles = ["../exp_data/04302021/tx_15_rx_0_lf.txt"]
    # expFiles = ["../exp_data/05082021/tx_1_5_10_15_0_rx_1_5_10_15_0.txt"]
    # expFiles = ["../exp_data/04232021/tx_[1,5,10,15,0]_rx_[1,5,10,15,0].txt", "../exp_data/04242021/tx_[1,5,10,15,0]_rx_[1,5,10,15,0]_lf_5_1.txt", "../exp_data/05082021/tx_1_5_10_15_0_rx_1_5_10_15_0.txt", "../exp_data/05092021/tx_rx_full_sweep_rev_lf.txt", "../exp_data/05102021/tx_rx_full_sweep_low_lf_1_5_hr.txt"]
    # expFiles = ["../exp_data/05102021/tx_rx_full_sweep_rev.txt"]
    # expFiles = ["../exp_data/04232021/tx_[1,5,10,15,0]_rx_[1,5,10,15,0].txt"]
    expFiles = ["../exp_data/tx_rx_data.txt"]
    
    # expFiles = ["../exp_data/04262021/tx_rx_lmp10_boottime.txt"]
    # expDates = ["05062021-noacks1", "05062021-noacks3", "05062021-noacks5"]
    # expDates = ["04232021", "04242021", "05082021", "05092021", "05102021"]
    # expDates = [ "04232021"]
    expDates = [ "05112021"]
    # senderLMPs = [1, 5, 10, 15, 0] 
    # receiverLMPs = [1, 5, 1, 5, 10, 0, 5, 0, 5]

    ar.runFullSweepAnalysis(expFiles, expDates, printToPdf=True)
    # ar.runTxSweepAnalysis(expFiles, senderLMPs, receiverLMPs, expDates, printToPdf=False)
    # ar.runBootDelayAnalysis(expFiles, expDates, printToPdf=True)
    # ar.appendMetrics(expFiles, expDates, medianConfig=True, meanConfig=False, printToPdf=True)


if __name__ == "__main__":
    main()
