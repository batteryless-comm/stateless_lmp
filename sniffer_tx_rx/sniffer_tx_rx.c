#include <msp430.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

/*
 * 5 min - 36621
 * 1 min - 7325
 * 30 sec - 3663
 */
#define EXP_RESOLUTION          (36621)
#define TOTAL_TIME_UNITS        (18)
#define NUM_EXP                 (25)

uint16_t num_pkts_rx = 0, num_pkts_tx = 0, num_acks_rec = 0, num_acks_sent = 0;
uint16_t tmp_TB0CCR5 = 0, tmp_TB0CCR6 = 0, num_timeUnits = 0;
uint16_t ovf_count_TA = 0, ovf_count_TB = 0;
uint8_t power_rx, power_tx, sendIntRxFlag = 0, sendIntTxFlag = 0, numRxEdgeCount = 0, numTxEdgeCount = 0;
//uint16_t num_lifecycle = 0;

char readstr[20];

void clk_setup() {
    // Init clocks and I/O:
    // Startup clock system with max DCO setting ~8MHz
    CSCTL0_H = CSKEY >> 8;                      // CSKey=A500.  Unlock clock registers
    CSCTL1 = DCOFSEL_6;                         // Set DCO to 8MHz.  6|40
    CSCTL2 = SELA__VLOCLK | SELS__DCOCLK | SELM__DCOCLK; //Set SMCLK = DC0
    CSCTL3 = DIVA__1 | DIVS__1 | DIVM__1;       // Set all dividers
    CSCTL0_H = 0;                               // Lock CS registers
}

void UART_config() {
        // setup pin TX 2.0 and RX 2.1 for back-channel UART
        P2SEL0 &= ~(BIT0 | BIT1);
        P2SEL1 |= BIT0 | BIT1;
        UCA0CTLW0 = UCSWRST;                      // Put eUSCI in reset
        UCA0CTLW0 |= UCSSEL__SMCLK;               // CLK = SMCLK
        UCA0BRW = 4;                             // 8000000/16/115200
        UCA0MCTLW |= UCOS16 | UCBRF_5 | 0x55;
        UCA0CTLW0 &= ~UCSWRST;
}

void UART_TX(char * tx_data)                 // Define a function which accepts a character pointer to an array
{
    unsigned int i=0;
    while(tx_data[i])                        // Increment through array, look for null pointer (0) at end of string
    {
        while ((UCA0STATW & UCBUSY));        // Wait if line TX/RX module is busy with data
        UCA0TXBUF = tx_data[i];              // Send out element i of tx_data array on UART bus
        i++;                                 // Increment variable for array address
    }
}

void timerB_init() {//TB0.1 on P1.4 -> CCR1 capture: CCI1A
    //Capture rising edges, capture compare port A(CCI1A), synchronous capture, enable capture mode, and enable interrupts
//    TB0CCTL0 = CM__RISING | CCIS_0 | SCS | CAP | CCIE;
    TB0CCTL1 = CM__RISING | CCIS_0 | SCS | CAP | CCIE;
    TB0CCTL2 = CM__RISING | CCIS_0 | SCS | CAP | CCIE;
    TB0CCTL3 = CM__RISING | CCIS_0 | SCS | CAP | CCIE;
    TB0CCTL4 = CM__RISING | CCIS_0 | SCS | CAP | CCIE;
    //Capture both edge for power on-time and off-time
    TB0CCTL5 = CM__BOTH | CCIS_0 | SCS | CAP | CCIE;
    TB0CCTL6 = CM__BOTH | CCIS_0 | SCS | CAP | CCIE;

    // Use select SMCLK, continous mode, reset timer, and enable interrupts
    TB0CTL = TBSSEL__SMCLK | MC__CONTINUOUS | TBCLR | TBIE;
}

void timerA_init() {    // Timer A used for keeping track of run time of experiment
//    TA1CCTL2 = CM__RISING | CCIS_0 | SCS | CAP | CCIE;
                                           // Capture rising edge,
                                           // Use CCIAB=PIN1.3,
                                           // Synchronous capture,
                                           // Enable capture mode,
                                           // Enable capture interrupt

    TA1CTL = TASSEL__SMCLK | MC__CONTINUOUS | TACLR | TAIE;// Use SMCLK as clock source,
                                           // Start timer in continuous mode
}

void gpio_config() {
    // Configure P1.0 and P1.1 for LED output
    P1OUT &= ~BIT0; //P1.0 output it low
    P1DIR |= BIT0; //P1.0 is output
    P1OUT &= ~BIT1; //P1.1 output it low
    P1DIR |= BIT1; //P1.1 is output
    P4OUT &= ~BIT3; //P4.3 output it low
    P4DIR |= BIT3; //P4.3 is output
    P4OUT &= ~BIT2; //P4.3 output it low
    P4DIR |= BIT2; //P4.3 is output

    // Configure P1.4, P1.5 for Timer B 0.1, 0.2 capture
    P1DIR &=  ~(BIT4 | BIT5); //P1.4 and P1.5 is input
    P1OUT &= ~(BIT4 | BIT5); //P1.4 and P1.5 has a pull-down resistor
    P1REN |= (BIT4 | BIT5);
    P1SEL0 |=  (BIT4 | BIT5); //P1.4 and P1.5 alternate functions on TB0.2
    P1SEL1 &= ~(BIT4 | BIT5);

    // Configure P3.4, P3.5, P3.6, and P3.7 for Timer B
    P3DIR &=  ~(BIT4 | BIT5 | BIT6 | BIT7); //P3.4, P3.5, P3.6, and P3.7 is input
    P3OUT &= ~(BIT4 | BIT5 | BIT6 | BIT7); //P3.4, P3.5, P3.6, and P3.7 has a pull-down resistor
    P3REN |= (BIT4 | BIT5 | BIT6 | BIT7);
    P3SEL0 |=  (BIT4 | BIT5 | BIT6 | BIT7); //P3.4, P3.5, P3.6, and P3.7 alternate functions are TB0.3, TB0.4, TB0.5, and TB0.6
    P3SEL1 &= ~(BIT4 | BIT5 | BIT6 | BIT7);

    // Configure P5.6 for button input
    P5DIR &= ~BIT6; //P5.6 is input
    P5OUT |= BIT6; //P5.6 has a pull-up resistor
    P5REN |= BIT6;

    // Configure GPIO interrupt on P3.2, P3.3
    P3IES &= ~BIT2;             // P3.2 lo to hi edge
    P3IFG &= ~BIT2;             // P3.2 IFG cleared
    P3IE |= BIT2;               // P3.2 interrupt enabled
    P3IES &= ~BIT3;             // P3.3 lo to hi edge
    P3IFG &= ~BIT3;             // P3.3 IFG cleared
    P3IE |= BIT3;               // P3.3 interrupt enabled

    // Disable the GPIO power-on default high-impedance mode to activate
    // previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;
}

void sendIntRx() {
    // Send the interrupt to sender to change the fast die config
    __delay_cycles(40000);
    P4OUT ^= BIT3;
    __delay_cycles(80000);
    P4OUT ^= BIT3;
}

void sendIntTx() {
    // Send the interrupt to sender to change the fast die config
    __delay_cycles(40000);
    P4OUT ^= BIT2;
    __delay_cycles(80000);
    P4OUT ^= BIT2;
}

int main(void)
{
    uint8_t expCount = 0;
    uint8_t numExp = NUM_EXP;
    uint8_t txConfigCount = 0;

    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    clk_setup(); // Setup clk to run at 8 MHz
    UART_config();  // UART configured at 115200 baud rate
    gpio_config();
    timerB_init();
    timerA_init();

//    __enable_interrupt();

    while(1) {
        // Check for button press P5.6
        if(~P5IN & BIT6) {
            P1OUT &= ~BIT0; // clear red led

            sprintf(readstr, "New Experiment!\n");
            UART_TX(readstr);

            while(1){
                // Go into LPM0 mode and wait for an interrupt
                __bis_SR_register(LPM0_bits | GIE); //enter LPM

                // Send data to UART
                // Check for 5 mins
                if(ovf_count_TA >= EXP_RESOLUTION) {
                    num_timeUnits++;
                    ovf_count_TA = 0;
                }

                // When rx edge is detected
                if(power_rx == 1){

                    if(sendIntRxFlag){
//                        UART_TX("Interrupt RX sent\n");
                        sendIntRx();
//                        numRxEdgeCount++;
//                        if(numRxEdgeCount >= 2){
//                            sendIntRxFlag = 0;
//                            numRxEdgeCount = 0;
//                        }
                    }
                    UART_TX("Power RX: ");
                    sprintf(readstr, "%x", tmp_TB0CCR5);
                    UART_TX(readstr);
                    UART_TX(",");
                    sprintf(readstr, "%d", ovf_count_TB);
                    UART_TX(readstr);
                    UART_TX(",");
                    sprintf(readstr, "%d", num_pkts_rx);
                    UART_TX(readstr);
                    UART_TX(",");
                    sprintf(readstr, "%d", num_acks_sent);
                    UART_TX(readstr);
                    UART_TX("\n");

                    num_pkts_rx = 0;
                    num_acks_sent = 0;
                    power_rx = 0;
                    ovf_count_TB = 0;
                }

                // when tx edge is detected
                if(power_tx == 1) {
                    if(sendIntTxFlag){
//                        UART_TX("Interrupt TX sent\n");
                        sendIntTx();
//                        numTxEdgeCount++;
//                        if(numTxEdgeCount >= 2){
//                            sendIntTxFlag = 0;
//                            numTxEdgeCount = 0;
//                        }
                    }
                    UART_TX("Power TX: ");
                    sprintf(readstr, "%x", tmp_TB0CCR6);
                    UART_TX(readstr);
                    UART_TX(",");
                    sprintf(readstr, "%d", ovf_count_TB);
                    UART_TX(readstr);
                    UART_TX(",");
                    sprintf(readstr, "%d", num_pkts_tx);
                    UART_TX(readstr);
                    UART_TX(",");
                    sprintf(readstr, "%d", num_acks_rec);
                    UART_TX(readstr);
                    UART_TX("\n");

                    num_pkts_tx = 0;
                    num_acks_rec = 0;
                    power_tx = 0;
                    ovf_count_TB = 0;
                }

                if(num_timeUnits >= TOTAL_TIME_UNITS){
                    ++expCount;
                    sprintf(readstr, "Experiment Completed: %d\n", expCount);
                    UART_TX(readstr);
                    // Send the interrupt to sender and receiver to change the fast die config
                    sendIntTxFlag = 1;
//                    sendIntRxFlag = 1;

//                    UART_TX("Interrupt TX sent\n");
                    txConfigCount++;


                    // When sender does the full sweep of config
                    if(txConfigCount >= 5) {
                        sendIntRxFlag = 1;
                        txConfigCount = 0;
//                        UART_TX("Interrupt RX sent\n");
                    }

                    ovf_count_TA = 0;
                    num_timeUnits = 0;
                    // The whole set of experiments are done
                    if(expCount >= numExp) {
                        P1OUT |= BIT0;
                        while(1);
                    }
                }
            }

        }
    }
}


//  TA1 Interrupt Handler
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = TIMER1_A1_VECTOR
__interrupt void Timer1_A1_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER1_A1_VECTOR))) Timer1_A1_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch (__even_in_range(TA1IV, TAIV__TAIFG))
    {
        case TAIV__TACCR1: break;
        case TAIV__TACCR2: break;
        case TAIV__TAIFG:
            ovf_count_TA++; // each overflow corresponds to 8.192 ms when clk rate is 8MHz
            __bic_SR_register_on_exit(LPM0_bits);
            break;
        default: break;
    }
}

// TimerB Interrupt Vector (TBIV) handler
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER0_B1_VECTOR
__interrupt void TIMER0_B1_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER0_B1_VECTOR))) TIMER0_B1_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch(__even_in_range(TB0IV, TBIV__TBIFG))
    {
        case TBIV__NONE:    break;          // No interrupt
        case TBIV__TBCCR1:
            num_pkts_rx++;  // increment pkts received
            break;
        case TBIV__TBCCR2:
            num_acks_sent++;
            break;           // TB0CCR2 interrupt for acks sent by the receiver
        case TBIV__TBCCR3:
            num_pkts_tx++;
            break;           // TB0CCR3 interrupt for number of packets transmitted
        case TBIV__TBCCR4:
            num_acks_rec++;
            break;           // TB0CCR4 interrupt for number of ack received at TX
        case TBIV__TBCCR5:
            tmp_TB0CCR5 = TBCCR5;
            power_rx = 1;
            // Exit from LPM0 and continue executing main
            __bic_SR_register_on_exit(LPM0_bits);
            break;           // TB0CCR5 interrupt for monitoring power on RX
        case TBIV__TBCCR6:
            tmp_TB0CCR6 = TBCCR6;
            power_tx = 1;
            // Exit from LPM0 and continue executing main
            __bic_SR_register_on_exit(LPM0_bits);
            break;           // TB0CCR6 interrupt for monitoring power on TX
        case TBIV__TBIFG:
            ovf_count_TB++;
            break;                 // overflow
        default: break;
    }
}

// Port 3 interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT3_VECTOR
__interrupt void port3_isr_handler(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(PORT1_VECTOR))) port3_isr_handler (void)
#else
#error Compiler not supported!
#endif
{
    switch(__even_in_range(P3IV, P3IV__P3IFG7))
    {
        case P3IV__NONE:    break;          // Vector  0:  No interrupt
        case P3IV__P3IFG2:
            sendIntTxFlag = 0;        // Clear send interrupt Tx flag
            P3IFG &= ~BIT2;           // clear the interrupt flag
            __bic_SR_register_on_exit(LPM0_bits);
            break;
        case P3IV__P3IFG3:
            sendIntRxFlag = 0;        // Clear send interrupt Rx flag
            P3IFG &= ~BIT3;           // clear the interrupt flag
            __bic_SR_register_on_exit(LPM0_bits);
            break;
        default: break;
    }
}

