# LMP Implementation on MSP430FR5994 and CC1352R
This project implement stateless Lifecycle Management Protocol (LMP) and explores how different LMP configuration affects the probability of communication. The article is published at 2021 IEEE 18th International Conference on Mobile Ad Hoc and Smart Systems (MASS) and can be found at this [link](https://ieeexplore.ieee.org/document/9637119). This project use MSP430FR5994 as the main application processor with CC1352R as radio board. The clock is emulated by another MSP430FR5994 board.

The paper can be cited using the [ieee link](https://ieeexplore.ieee.org/document/9637119) or using following -

V. Deep et al., "Experimental Study of Lifecycle Management Protocols for Batteryless Intermittent Communication," 2021 IEEE 18th International Conference on Mobile Ad Hoc and Smart Systems (MASS), 2021, pp. 355-363, doi: 10.1109/MASS52906.2021.00052.

`
@INPROCEEDINGS{deeplmp2021,
  author={Deep, Vishal and Wymore, Mathew L. and Aurandt, Alexis A. and Narayanan, Vishak and Fu, Shen and Duwe, Henry and Qiao, Daji},
  booktitle={2021 IEEE 18th International Conference on Mobile Ad Hoc and Smart Systems (MASS)}, 
  title={Experimental Study of Lifecycle Management Protocols for Batteryless Intermittent Communication},
  year={2021},
  volume={},
  number={},
  pages={355-363},
  doi={10.1109/MASS52906.2021.00052}}
`

# System setup for MSP430 running TI-RTOS
- Install MSP430 TI-RTOS version - 2.20.0.06. After installing, it could be added to ccs from Window->Preferences->Code Composer Studio-> Compiler.
- [XDCTools version - 3.32.0.06_core](http://downloads.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/rtsc/)    
- [MSP430 Compiler tools version - 16.9.11.LTS](http://software-dl.ti.com/ccs/esd/documents/ccs_compiler-installation-selection.html)
    To install a specific compiler tools version:
    In CCS got to Help -> Install Code generation Compiler Tools -> TI Compiler Updates -> Choose a version and install
    To select a compiler version:
    Right click on project -> properties -> Resource -> CCS General -> Choose compiler version
Note: If you have MSP430 compiler v20.12 issue while importing, Please install it from this [link](https://www.ti.com/tool/download/MSP-CGT-21/20.12.0.STS).

# Peripherals Setup
All the examples/steps are specifically for MSP430FR5994
## 1. Adding a TI-RTOS driver to MSP430
By default TI-RTOS does not include all drivers. We need to perform the steps given in [TI-RTOS user manual section 5.2.8](https://www.ti.com/lit/ug/spruhd4m/spruhd4m.pdf?ts=1613072721257&ref_url=https%253A%252F%252Fwww.google.com%252F) for MSP430 devices to add a TI-RTOS driver such as UART, SPI, I2C etc.
## 2. GPIO
To add a GPIO, go to `<board>.c` and add the GPIO pin that needs to be configured in `gpioPinCofig[]` array as shown below. The name of the pin can be found in `GPIOMSP430.h`. 
If using an interrupt in the GPIO pin, update the `gpioCallbackFunctions[]` also with the callback function name or `NULL` if declaring dynamically using `GPIO_setConfig()`.
```
GPIO_PinConfig gpioPinConfigs[] = {
    /* Input pins */
    /* MSP_EXP430FR5994_S1 */
    GPIOMSP430_P5_6 | GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_RISING,

    /* Output pins */
    /* MSP_EXP430FR5994_LED1 */
    GPIOMSP430_P1_0 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH | GPIO_CFG_OUT_LOW,
    /* MSP_EXP430FR5994_LED2 */
    GPIOMSP430_P7_0 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH | GPIO_CFG_OUT_LOW,
};
```
Then in `<board>.h` add it to the enum of GPIOName:
```
typedef enum MSP_EXP430FR5994_GPIOName {
    MSP_EXP430FR5994_S1 = 0,
    MSP_EXP430FR5994_LED1,
    MSP_EXP430FR5994_P7_0,

    MSP_EXP430FR5994_GPIOCOUNT
} MSP_EXP430FR5994_GPIOName;
```
After that in `Board.h` define a name of the GPIO pin that you just configured:
```
#define Board_P7_0                  MSP_EXP430FR5994_P7_0
```
Then GPIO pin can be directly accessed in the code like this:
```
GPIO_toggle(Board_P7_0);
```
## 2. UART
- In CCS project properties -> Resource -> CCS General -> MPU tab -> uncheck Enable Memory Protection Unit (MPU).
- Open .cfg file in the project and add following lines at the end or use the method described in 1:
```
var hwiParams = new halHwi.Params();
hwiParams.arg = 0;
halHwi.create(48, "&UARTEUSCIA_hwiIntFxn", hwiParams);
```
## 3. SPI
- Open .cfg file in the project and add following lines at the end or use the method described in 1. 42 is interrupt vector controller number for DMA.
```
var halHwi1Params = new halHwi.Params();
halHwi1Params.instance.name = "halHwi1";
halHwi1Params.arg = 0;
halHwi1Params.priority = 5;
Program.global.halHwi1 = halHwi.create(42, "&MSP_EXP430FR5994_isrDMA", halHwi1Params); 
```
In `MSP_EXP430FR5994.C` update the DMA channel and index according to the board specifications given in [Table 9-11 of the datasheet](https://www.ti.com/lit/ds/symlink/msp430fr5994.pdf?ts=1614090323923&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FMSP430FR5994).
```
const SPIEUSCIADMA_HWAttrs spiEUSCIADMAHWAttrs[MSP_EXP430FR5994_SPICOUNT] = {
    {
        .baseAddr = EUSCI_A3_BASE,
        .clockSource = EUSCI_A_SPI_CLOCKSOURCE_SMCLK,
        .bitOrder = EUSCI_A_SPI_MSB_FIRST,
        .scratchBufPtr = &spiEUSCIADMAscratchBuf[0],
        .defaultTxBufValue = 0,

        /* DMA */
        .dmaBaseAddr = DMA_BASE,
        /* Rx Channel */
        .rxDMAChannelIndex = DMA_CHANNEL_4,
        .rxDMASourceTrigger = DMA_TRIGGERSOURCE_16,
        /* Tx Channel */
        .txDMAChannelIndex = DMA_CHANNEL_5,
        .txDMASourceTrigger = DMA_TRIGGERSOURCE_17
    }
};
```

### CC1352R and MSP430FR5994 SPI Pin Config
| CC1352R (Peripheral) | MSP430FR5994 (Controller) | Function     |
| ------- | ------------ | --------     |
|  DIO10  | P6.2         | CLK          |  
|  DIO8   | P6.1         | MISO         |  
|  DIO9   | P6.0         | MOSI         |
|  DIO20  | P6.3         | Peripheral Select |
|  DIO21  | P4.2         | Peripheral ready  |
|  DIO15  | P4.1         | Controller ready | 
|  3V3    | P3.6         | Power radio  |
|  GND    | GND          | Ground       |

Note: When powering CC1352R from MSP430 board, make sure to have GPIO strength high.

## 4. I2C
Open .cfg file in the project and add following lines at the end or use the method described in 1:
```
var halHwi1Params = new halHwi.Params();
halHwi1Params.instance.name = "halHwi1";
halHwi1Params.arg = 0;
halHwi1Params.priority = 5;
Program.global.halHwi1 = halHwi.create(23, "&I2CEUSCIB_hwiIntFxn", halHwi1Params); 
```
### MSP430FR5994 and MSP430FR5994 I2C Pin Config
Please make sure both pins of I2C are pulled up with a 10K resistor.
| MSP430FR5994 (Controller) | MSP430FR5994 (Peripheral) | Function     |
| ------- | ------------ | --------     |
|  P5.0   | P5.0         | SDA          |  
|  P5.1   | P5.1         | SCL          |  

# Sniffer MSP430 setup
The sniffer MSP430 can be used to track the transmitted and received packets. The pin configuration is given below:
| Sniffer (MSP430) |     TX     |     RX     |       Fnx                        |
| ---------------- | ---------- | ---------- | -------------------------------- |
| P1.4 - TB0.1	   |        	|   DIO 18   |  Pkts received	                |
| P1.5 - TB0.2	   |        	|   DIO 4    |  Acks sent    	                |
| P3.4 - TB0.3     |   DIO 18   |            |  Number of packets transmitted   |
| P3.5 - TB0.4     |   DIO 19   |            |  Ack received                    |
| P3.6 - TB0.5     |            |   P3.7     |  Power RX                        |
| P3.7 - TB0.6     |   P3.7     |            |  Power TX                        |
| P4.2             |   P4.3     |            |  Fast Die config change          |
| P4.3             |            |   P4.3     |  Fast Die config change          |
| P3.2             |   P3.2     |            | Fast Die config change ACK Tx    |
| P3.3             |            |   P3.3     | Fast Die config change ACK Rx    |
| GND              |   GND      |   GND      |  Ground                          |
  

To run and record an experiment for particular duration, in [snifer_tx_rx.c](sniffer_tx_rx/sniffer_tx_rx.c) following preprocessors can be changed:
In this project, a 12-bit timer is used with 8MHz system clock.  

'EXP_RESOLUTION' is the minimum amount of time that you want to measure with the timer.  
'TOTAL_TIME_UNITS' is how long you want to run the experiment in terms of 'EXP_RESOLUTION'.   
'NUM_EXP' is how many such experiments you would like to run.  


```
#define EXP_RESOLUTION          (36621)
#define TOTAL_TIME_UNITS        (6)
#define NUM_EXP                 (1)
```

# MSP430 and RF Harvester setup
| MSP430FR5994 (TX) | PowerCast P2110 Harvester | Function     |
| ------- | ------------ | -------------  |
|  GND    | GND          | Ground         |  
|  3V3    | BATT         | Power supply   | 
|  P2.5   | Reset        | Fast-die reset |

# Experiment setup
1. The complete experiment setup images can be found here - [experiment setup images](exp_data/experiment-setup-images)
2. Please remove all the jumpers on the debugger side of the controller and radio board.
3. Make sure the starting LMP configuration is 1-1 on each node.

# LMP Protocols Configuration
0 = No fast die - NULL LMP  
1 = Fast die after 1 no ack  
.  
.  
n = Fast die after 'n' no acks  




