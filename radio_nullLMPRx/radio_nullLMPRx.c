#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Clock.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>
#include <ti/display/Display.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/Assert.h>
#include <xdc/runtime/Error.h>

/* Driver configuration */
#include "ti_drivers_config.h"

/* EasyLink API Header files */
#include "easylink/EasyLink.h"

/* Application header files */
#include <ti_radio_config.h>

#define DEBUG                           (0)

#define TASKSTACKSIZE                   (1024)
#define SPI_MSG_LENGTH                  (5)
#define RFEASYLINKTXPAYLOAD_LENGTH      (4)
#define MAX_LOOP                        (10)
#define fastDieConfigLength             (5)

static Display_Handle display;

Task_Struct radioRxTask;
static Task_Params radioRxTaskParams;
static  Char radioRxTaskStack[TASKSTACKSIZE];

uint8_t peripheralRxBuffer[SPI_MSG_LENGTH];
uint8_t peripheralTxBuffer[SPI_MSG_LENGTH];

/* Semaphore to block peripheral until transfer is complete */
static Semaphore_Handle peripheralSem;
static Semaphore_Handle rxDoneSem;

/* Status indicating whether or not SPI transfer succeeded. */
bool transferStatus;
bool updateFastDieConfig = false;

EasyLink_RxPacket rxPacketBuff =  { {0}, 0, 0, {0} };

void rxDoneCb(EasyLink_RxPacket * rxPacket, EasyLink_Status status) {
    uint8_t i;
    if (status == EasyLink_Status_Success) {
#if DEBUG
        GPIO_write(CONFIG_GPIO_LED_0, 1);
        GPIO_write(CONFIG_GPIO_LED_0, 0);
#endif
        // Copy the rx packet to a buffer
        for(i=0; i<rxPacket->len; i++){
            rxPacketBuff.payload[i] = rxPacket->payload[i];
        }
    }

    Semaphore_post(rxDoneSem);
}
void txAckDoneCb(EasyLink_Status status) {
    if (status == EasyLink_Status_Success) {
#if DEBUG
        GPIO_write(CONFIG_GPIO_LED_1, 1);
        GPIO_write(CONFIG_GPIO_LED_1, 0);
#endif
    }

    Semaphore_post(rxDoneSem);
}

/* Callback function for SPI_transfer(). */
void transferCompleteFxn(SPI_Handle handle, SPI_Transaction *transaction) {
    if (transaction->status != SPI_TRANSFER_COMPLETED) {
        transferStatus = false;
    } else {
        transferStatus = true;
    }

    Semaphore_post(peripheralSem);
}

/* Callback function for the GPIO interrupt on DIO5*/
void setFastDieConfigFxn(uint_least8_t index) {
    updateFastDieConfig = true;  // update fast-die config change to true
}

/* Sends payload fetched from MSP430 controller via radio to other node */
void radioRxFxn(UArg arg0, UArg arg1) {
    SPI_Handle      peripheralSpi;
    SPI_Params      spiParams;
    SPI_Transaction transaction;
    bool            transferOK;
    uint8_t         i;
    uint8_t         rxStatus;
    uint32_t        rxTimeOut = 0; // defaulted to Null-LMP
    uint32_t        timeOutSuccTx = 0;
    uint32_t        timeout = 0;
    bool            succOnTime = false;

    /* Semaphore params */
    Semaphore_Params params;
    Error_Block eb;
    /* Init params */
    Semaphore_Params_init(&params);
    Error_init(&eb);

    // Initialize the EasyLink parameters to their default values
    EasyLink_Params easyLink_params;
    EasyLink_Params_init(&easyLink_params);


    if(EasyLink_init(&easyLink_params) != EasyLink_Status_Success) {
#if DEBUG
        Display_printf(display, 0, 0, "SEasyLink_init failed\n");
#endif
        while(1);
    }


    /* Set TX Power to 0dBm */
//    EasyLink_setRfPower(0);

    /*
     * Below we set CONFIG_SPI_CONTROLLER_READY & CONFIG_SPI_RADIO_READY initial
     * conditions for the 'handshake'.
     */
    GPIO_setConfig(CONFIG_SPI_RADIO_READY, GPIO_CFG_OUTPUT | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_SPI_CONTROLLER_READY, GPIO_CFG_INPUT);

    /*
     * Handshake - Set CONFIG_SPI_RADIO_READY high to indicate peripheral is ready
     * to run.  Wait for CONFIG_SPI_CONTROLLER_READY to be high.
     */
    GPIO_write(CONFIG_SPI_RADIO_READY, 1);
//    Display_printf(display, 0, 0, "CONFIG_SPI_RADIO_READY\n");
    while (GPIO_read(CONFIG_SPI_CONTROLLER_READY) == 0) {}


    /*
     * Create synchronization semaphore; this semaphore will block the peripheral
     * until a transfer is complete.
     */
    /* Create semaphore instance */
    peripheralSem = Semaphore_create(0, &params, &eb);
    /* Create a semaphore for Async TX */
    rxDoneSem  = Semaphore_create(0, &params, &eb);

    if(peripheralSem == NULL) {
//       Display_printf(display, 0, 0, "Semaphore creation failed\n");
       while(1);
    }
    if(rxDoneSem  == NULL) {
//        Display_printf(display, 0, 0, "Semaphore creation failed\n");
        while(1);
    }

    /* Wait until controller SPI is open.  */
    while (GPIO_read(CONFIG_SPI_CONTROLLER_READY)) {}
//    Display_printf(display, 0, 0, "CONFIG_SPI_CONTROLLER_READY\n");


    /*
     * Open SPI as peripheral in callback mode; callback mode is used to allow us to
     * configure the transfer & then set CONFIG_SPI_RADIO_READY high.
     */
    SPI_Params_init(&spiParams);
    spiParams.frameFormat = SPI_POL0_PHA1;
    spiParams.mode = SPI_SLAVE;
    spiParams.transferCallbackFxn = transferCompleteFxn;
    spiParams.transferMode = SPI_MODE_CALLBACK;
    peripheralSpi = SPI_open(CONFIG_SPI_SLAVE, &spiParams);

#if DEBUG
    if (peripheralSpi == NULL) {
        Display_printf(display, 0, 0, "Error initializing peripheral SPI\n");
        while (1);
    } else {
        Display_printf(display, 0, 0, "Peripheral SPI initialized\n");
    }
#endif

    /* Initialize peripheral SPI transaction structure */
     memset((void *) peripheralRxBuffer, 0, SPI_MSG_LENGTH);
     transaction.count = SPI_MSG_LENGTH;
     transaction.txBuf = (void *) peripheralTxBuffer;
     transaction.rxBuf = (void *) peripheralRxBuffer;


     // send the fast-die config to radio board for first time after boot-up
     updateFastDieConfig = true;

    while(1) {
        // Check for fast-die config change
        if(updateFastDieConfig){    // ToDo: run only till fastDieConfigLength
            // Receive time out info from the controller
            transferOK = SPI_transfer(peripheralSpi, &transaction);

            if (transferOK) {
               GPIO_write(CONFIG_SPI_RADIO_READY, 0);

               /* Wait until transfer has completed */
               Semaphore_pend(peripheralSem, BIOS_WAIT_FOREVER);
               /*
                * Drive CONFIG_SPI_PERPHERAL_READY high to indicate radio is not ready
                * for another transfer yet.
                */
               GPIO_write(CONFIG_SPI_RADIO_READY, 1);
               if (transferStatus == true) {
                   rxTimeOut = peripheralRxBuffer[0] * 1000; // us
//                   Display_printf(display, 0, 0, "timeout = %lu\n", rxTimeOut);
                   timeOutSuccTx = peripheralRxBuffer[1] * 1000; // us
               }
            }
            updateFastDieConfig = false;
        }

        /* Receive payload from sender */
        EasyLink_receiveAsync(rxDoneCb, 0);

        if (rxTimeOut == 0){
            // NULL LMP
            /* Wait indefinitely for Rx */
            Semaphore_pend(rxDoneSem, BIOS_WAIT_FOREVER);
            rxStatus = 1;
        } else {
            // FAST-LMP with fast-die config
            // If it's a successful on-time then change the fast-die config to 1
            if (succOnTime) {
                timeout = timeOutSuccTx;
            } else {
                timeout = rxTimeOut;
            }
            /* Wait for Rx to complete */
            if(Semaphore_pend(rxDoneSem, (timeout / Clock_tickPeriod)) == FALSE) {
                rxStatus = 0;
                /* RX timed out, abort */
                if(EasyLink_abort() == EasyLink_Status_Success) {
                    /* Abort will cause the rxAckDoneCb to be called */
                   Semaphore_pend(rxDoneSem, BIOS_WAIT_FOREVER);
                }
            } else {
                rxStatus = 1;
                succOnTime = true;
            }
        }


#if DEBUG
        Display_printf(display, 0, 0, "Rx status = %d\n", rxStatus);
#endif

        if(rxStatus){
            /*
             * When the packet is  received from the sender
             */
            EasyLink_TxPacket txPacket =  { {0}, 0, 0, {0} };

#if DEBUG
            // Start a SPI transfer to main controller
            /* Toggle on user LED, indicating a SPI transfer is in progress */
            GPIO_toggle(CONFIG_GPIO_LED_1);
#endif

            /* Copy Rx data to SPI transmit buffer */
            for(i=0; i<SPI_MSG_LENGTH; i++){
                peripheralTxBuffer[i] = rxPacketBuff.payload[i];
            }
            // append the rxStatus to the SPI message
            peripheralTxBuffer[SPI_MSG_LENGTH-1] = rxStatus;

            /* Setup SPI transfer; CONFIG_SPI_SLAVE_READY will be set to notify
            * controller that the peripheral is ready.*/
            // send the received data to the controller
            transferOK = SPI_transfer(peripheralSpi, &transaction);

#if DEBUG
            Display_printf(display, 0, 0, "Transfer OK = %d\n", transferOK);
#endif

            if (transferOK) {
               GPIO_write(CONFIG_SPI_RADIO_READY, 0);

               /* Wait until transfer has completed */
               Semaphore_pend(peripheralSem, BIOS_WAIT_FOREVER);

               /*
                * Drive CONFIG_SPI_PERPHERAL_READY high to indicate slave is not ready
                * for another transfer yet.
                */
               GPIO_write(CONFIG_SPI_RADIO_READY, 1);
#if DEBUG
            Display_printf(display, 0, 0, "Transfer status = %d\n", transferStatus);
#endif
               if (transferStatus == true) {
                   // Notify that the packet is received and transferred to the MSP430
                   GPIO_write(CONFIG_GPIO_PKT_RX, 1);
                   GPIO_write(CONFIG_GPIO_PKT_RX, 0);

                   // Send ACK back to sender
                   /* Create packet with 0x00000000 -dummy ack payload*/
                   txPacket.payload[0] = 0x00;
                   txPacket.payload[1] = 0x01;
                   txPacket.payload[2] = 0x02;
                   txPacket.payload[3] = 0x03;

                   txPacket.len = 4;
                   /*
                    * Address filtering is enabled by default on the Rx device with the
                    * an address of 0xAA. This device must set the dstAddr accordingly.
                    */
                   txPacket.dstAddr[0] = 0xaa;
                   /* send now */
                   txPacket.absTime = 0;

                   EasyLink_transmitAsync(&txPacket, txAckDoneCb);
                   /* Wait for Tx to complete. A Successful TX will cause the TxDoneCb
                    * to be called and the rxDoneSem to be released, so we must
                    * consume the rxDoneSem
                    */
                   Semaphore_pend(rxDoneSem, BIOS_WAIT_FOREVER);

                   /* Toggle ack sent gpio to notify ack is sent */
                   GPIO_write(CONFIG_GPIO_ACK_SENT, 1);
                   GPIO_write(CONFIG_GPIO_ACK_SENT, 0);

               } else {
#if DEBUG
                   Display_printf(display, 0, 0, "SPI transfer failed!");
#endif
               }
            } else {
#if DEBUG
                // Unsuccessful slave SPI transfer
               Display_printf(display, 0, 0, "Unsuccessful peripheral SPI transfer");
#endif
            }
        } else {
            /*
             * When the packet is not received from the sender
             */
            // Clear successful on-time flag
            succOnTime = false;
#if DEBUG
            // Start a SPI transfer to main controller
            /* Toggle on user LED, indicating a SPI transfer is in progress */
            GPIO_toggle(CONFIG_GPIO_LED_1);
#endif

            /* Copy 0's to SPI transmit buffer indicating packet was not received*/
            for(i=0; i<SPI_MSG_LENGTH; i++){
                peripheralTxBuffer[i] = 0;
            }
            // append the rxStatus to the SPI message
            peripheralTxBuffer[SPI_MSG_LENGTH-1] = rxStatus;

#if DEBUG
            // Send rx data to the main controller
            Display_printf(display, 0, 0, "Send rx data to the main controller\n");
#endif

            /* Setup SPI transfer; CONFIG_SPI_SLAVE_READY will be set to notify
            * controller that the peripheral is ready.*/
            // send the rx status to the controller
            transferOK = SPI_transfer(peripheralSpi, &transaction);

#if DEBUG
            Display_printf(display, 0, 0, "Transfer OK = %d\n", transferOK);
#endif

            if (transferOK) {
               GPIO_write(CONFIG_SPI_RADIO_READY, 0);

               /* Wait until transfer has completed */
               Semaphore_pend(peripheralSem, BIOS_WAIT_FOREVER);

               /*
                * Drive CONFIG_SPI_PERPHERAL_READY high to indicate peripheral is not ready
                * for another transfer yet.
                */
               GPIO_write(CONFIG_SPI_RADIO_READY, 1);
            } else {
#if DEBUG
                // Unsuccessful slave SPI transfer
               Display_printf(display, 0, 0, "Unsuccessful peripheral SPI transfer");
#endif
            }
        }
    }
}

void radioRxTask_init() {
    Task_Params_init(&radioRxTaskParams);
    radioRxTaskParams.stackSize = TASKSTACKSIZE;
    radioRxTaskParams.stack = &radioRxTaskStack;
    Task_construct(&radioRxTask, (Task_FuncPtr)radioRxFxn, &radioRxTaskParams, NULL);
}

int main(void) {
    /* Call driver init functions. */
    Board_initGeneral();
    Display_init();
    GPIO_init();
    SPI_init();

#if DEBUG
    /* Configure the LED pins */
    GPIO_setConfig(CONFIG_GPIO_LED_0, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_GPIO_LED_1, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
#endif

    // Power monitor pin set to high
    GPIO_setConfig(CONFIG_GPIO_POWER_RX, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_HIGH);

    // Enable interrupt on DIO 5
    GPIO_setConfig(CONFIG_GPIO_FAST_DIE, GPIO_CFG_INPUT | GPIO_CFG_IN_PD | GPIO_CFG_IN_INT_RISING);
    /* install GPIO callback */
    GPIO_setCallback(CONFIG_GPIO_FAST_DIE, setFastDieConfigFxn);
    /* Enable interrupts */
    GPIO_enableInt(CONFIG_GPIO_FAST_DIE);

    /* Open the display for output */
    display = Display_open(Display_Type_UART, NULL);
    if (display == NULL) {
        /* Failed to open display driver */
        while (1);
    }

    radioRxTask_init();

    /* Start BIOS */
    BIOS_start();

    return (NULL);
}
