/**
 * These arguments were used when this file was generated. They will be automatically applied on subsequent loads
 * via the GUI or CLI. Run CLI with '--help' for additional information on how to override these arguments.
 * @cliArgs --board "/ti/boards/CC1352R1_LAUNCHXL" --product "simplelink_cc13x2_26x2_sdk@4.40.04.04"
 * @versions {"data":"2021010520","timestamp":"2021010520","tool":"1.7.0+1746","templates":null}
 */

/**
 * Import the modules used in this configuration.
 */
const Display  = scripting.addModule("/ti/display/Display");
const Display1 = Display.addInstance();
const GPIO     = scripting.addModule("/ti/drivers/GPIO");
const GPIO1    = GPIO.addInstance();
const GPIO2    = GPIO.addInstance();
const GPIO3    = GPIO.addInstance();
const GPIO4    = GPIO.addInstance();
const GPIO5    = GPIO.addInstance();
const GPIO6    = GPIO.addInstance();
const GPIO7    = GPIO.addInstance();
const GPIO8    = GPIO.addInstance();
const RTOS     = scripting.addModule("/ti/drivers/RTOS");
const SPI      = scripting.addModule("/ti/drivers/SPI");
const SPI1     = SPI.addInstance();
const easylink = scripting.addModule("/ti/easylink/easylink");

/**
 * Write custom configuration values to the imported modules.
 */
Display1.$name                    = "CONFIG_Display_0";
Display1.$hardware                = system.deviceData.board.components.XDS110UART;
Display1.uart.$name               = "CONFIG_UART_0";
Display1.uart.txPinInstance.$name = "CONFIG_PIN_0";
Display1.uart.rxPinInstance.$name = "CONFIG_PIN_1";

GPIO1.$hardware         = system.deviceData.board.components["BTN-1"];
GPIO1.$name             = "CONFIG_SPI_CONTROLLER_READY";
GPIO1.pinInstance.$name = "CONFIG_PIN_4";

GPIO2.mode              = "Dynamic";
GPIO2.$name             = "CONFIG_SPI_RADIO_READY";
GPIO2.gpioPin.$assign   = "boosterpack.8";
GPIO2.pinInstance.$name = "CONFIG_PIN_5";

GPIO3.mode              = "Output";
GPIO3.pull              = "Pull Down";
GPIO3.$name             = "CONFIG_GPIO_PKT_RX";
GPIO3.gpioPin.$assign   = "boosterpack.36";
GPIO3.pinInstance.$name = "CONFIG_PIN_10";

GPIO4.mode               = "Output";
GPIO4.initialOutputState = "High";
GPIO4.$name              = "CONFIG_GPIO_POWER_RX";
GPIO4.gpioPin.$assign    = "boosterpack.19";
GPIO4.pinInstance.$name  = "CONFIG_PIN_12";

GPIO5.$name             = "CONFIG_GPIO_LED_0";
GPIO5.$hardware         = system.deviceData.board.components.LED_RED;
GPIO5.pull              = "Pull Down";
GPIO5.pinInstance.$name = "CONFIG_PIN_2";

GPIO6.$name             = "CONFIG_GPIO_LED_1";
GPIO6.mode              = "Output";
GPIO6.$hardware         = system.deviceData.board.components.LED_GREEN;
GPIO6.pull              = "Pull Down";
GPIO6.pinInstance.$name = "CONFIG_PIN_3";

GPIO7.$name             = "CONFIG_GPIO_ACK_SENT";
GPIO7.mode              = "Output";
GPIO7.pull              = "Pull Down";
GPIO7.pinInstance.$name = "CONFIG_PIN_11";

GPIO8.$name             = "CONFIG_GPIO_FAST_DIE";
GPIO8.interruptTrigger  = "Rising Edge";
GPIO8.pinInstance.$name = "CONFIG_PIN_13";

const CCFG              = scripting.addModule("/ti/devices/CCFG", {}, false);
CCFG.ccfgTemplate.$name = "ti_devices_CCFGTemplate0";

SPI1.$name                 = "CONFIG_SPI_SLAVE";
SPI1.mode                  = "Four Pin SS Active Low";
SPI1.$hardware             = system.deviceData.board.components.MX25R8035F;
SPI1.defaultTxBufferValue  = "0xFF";
SPI1.sclkPinInstance.$name = "CONFIG_PIN_6";
SPI1.misoPinInstance.$name = "CONFIG_PIN_7";
SPI1.mosiPinInstance.$name = "CONFIG_PIN_8";
SPI1.ssPinInstance.$name   = "CONFIG_PIN_9";

easylink.EasyLink_Phy_200kbps2gfsk                                   = true;
easylink.EasyLink_Phy_2_4_250kbps2gfsk                               = true;
easylink.EasyLink_Phy_Custom                                         = false;
easylink.radioConfigEasylinkPhy200kbps2gfsk.txPower                  = "-10";
easylink.radioConfigEasylinkPhy200kbps2gfsk.codeExportConfig.$name   = "ti_devices_radioconfig_code_export_param2";
easylink.radioConfigEasylinkPhy24250kbps2gfsk.codeExportConfig.$name = "ti_devices_radioconfig_code_export_param1";

/**
 * Pinmux solution for unlocked pins/peripherals. This ensures that minor changes to the automatic solver in a future
 * version of the tool will not impact the pinmux you originally saw.  These lines can be completely deleted in order to
 * re-solve from scratch.
 */
Display1.uart.uart.$suggestSolution       = "UART1";
Display1.uart.uart.txPin.$suggestSolution = "boosterpack.4";
Display1.uart.uart.rxPin.$suggestSolution = "boosterpack.3";
GPIO1.gpioPin.$suggestSolution            = "boosterpack.13";
GPIO5.gpioPin.$suggestSolution            = "boosterpack.39";
GPIO6.gpioPin.$suggestSolution            = "boosterpack.40";
GPIO7.gpioPin.$suggestSolution            = "boosterpack.9";
GPIO8.gpioPin.$suggestSolution            = "boosterpack.10";
SPI1.spi.$suggestSolution                 = "SSI0";
SPI1.spi.sclkPin.$suggestSolution         = "boosterpack.7";
SPI1.spi.misoPin.$suggestSolution         = "boosterpack.14";
SPI1.spi.mosiPin.$suggestSolution         = "boosterpack.15";
SPI1.spi.ssPin.$suggestSolution           = "boosterpack.38";
SPI1.spi.dmaRxChannel.$suggestSolution    = "DMA_CH3";
SPI1.spi.dmaTxChannel.$suggestSolution    = "DMA_CH4";
