#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Clock.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>
#include <ti/display/Display.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/Assert.h>
#include <xdc/runtime/Error.h>

/* Driver configuration */
#include "ti_drivers_config.h"

/* EasyLink API Header files */
#include "easylink/EasyLink.h"

/* Application header files */
#include <ti_radio_config.h>

#define DEBUG                           (0)

#define TASKSTACKSIZE                   (1024)
#define SPI_MSG_LENGTH                  (4)
#define RFEASYLINKTXPAYLOAD_LENGTH      (4)
#define MAX_LOOP                        (10)

static Display_Handle display;

Task_Struct radioTxTask;
static Task_Params radioTxTaskParams;
static  Char radioTxTaskStack[TASKSTACKSIZE];

uint8_t peripheralRxBuffer[SPI_MSG_LENGTH];
uint8_t peripheralTxBuffer[SPI_MSG_LENGTH];

/* Semaphore to block peripheral until transfer is complete */
static Semaphore_Handle peripheralSem;
static Semaphore_Handle txDoneSem;

/* Status indicating whether or not SPI transfer succeeded. */
bool transferStatus;

/* Callback function for TX done */
void txDoneCb(EasyLink_Status status) {
    if (status == EasyLink_Status_Success) {
#if DEBUG
        GPIO_write(CONFIG_GPIO_LED_0, 1);
        GPIO_write(CONFIG_GPIO_LED_1, 0);
#endif
        GPIO_write(CONFIG_GPIO_PKTS_TX, 1);
        GPIO_write(CONFIG_GPIO_PKTS_TX, 0);
    } else {
#if DEBUG
        GPIO_write(CONFIG_GPIO_LED_0, 0);
        GPIO_write(CONFIG_GPIO_LED_1, 1);
#endif
    }

    Semaphore_post(txDoneSem);
}

/* Callback function for ACK received  */
void rxAckDoneCb(EasyLink_RxPacket * rxPacket, EasyLink_Status status) {
    if ((status == EasyLink_Status_Success)) { //read payload in future
#if DEBUG
        GPIO_write(CONFIG_GPIO_LED_0, 1);
        GPIO_write(CONFIG_GPIO_LED_1, 0);
#endif
        GPIO_write(CONFIG_GPIO_ACKS_RX, 1);
        GPIO_write(CONFIG_GPIO_ACKS_RX, 0);
    } else {
#if DEBUG
        GPIO_write(CONFIG_GPIO_LED_0, 0);
        GPIO_write(CONFIG_GPIO_LED_1, 1);
#endif
    }

    Semaphore_post(txDoneSem);
}


/* Callback function for SPI_transfer(). */
void transferCompleteFxn(SPI_Handle handle, SPI_Transaction *transaction) {
    if (transaction->status != SPI_TRANSFER_COMPLETED) {
        transferStatus = false;
    } else {
        transferStatus = true;
    }

    Semaphore_post(peripheralSem);
}

/* Sends payload fetched from MSP430 controller via radio to other node */
void radioTxFxn(UArg arg0, UArg arg1) {
    SPI_Handle      peripheralSpi;
    SPI_Params      spiParams;
    SPI_Transaction transaction;
    uint32_t        i;
    bool            transferOK;
    uint8_t         ackStatus;
    uint16_t        txTimeOut = 3000; // us

    /* Semaphore params */
    Semaphore_Params params;
    Error_Block eb;
    /* Init params */
    Semaphore_Params_init(&params);
    Error_init(&eb);

    // Initialize the EasyLink parameters to their default values
    EasyLink_Params easyLink_params;
    EasyLink_Params_init(&easyLink_params);

    if(EasyLink_init(&easyLink_params) != EasyLink_Status_Success) {
#if DEBUG
        Display_printf(display, 0, 0, "SEasyLink_init failed\n");
#endif
    }

    /* Set TX Power to 0dBm */
//    EasyLink_setRfPower(0);

    /*
     * Below we set CONFIG_SPI_CONTROLLER_READY & CONFIG_SPI_RADIO_READY initial
     * conditions for the 'handshake'.
     */
    GPIO_setConfig(CONFIG_SPI_RADIO_READY, GPIO_CFG_OUTPUT | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_SPI_CONTROLLER_READY, GPIO_CFG_INPUT);

    Display_printf(display, 0, 0, "CONFIG_SPI_RADIO_READY\n");

    /*
     * Handshake - Set CONFIG_SPI_RADIO_READY high to indicate peripheral is ready
     * to run.  Wait for CONFIG_SPI_CONTROLLER_READY to be high.
     */
    GPIO_write(CONFIG_SPI_RADIO_READY, 1);
    while (GPIO_read(CONFIG_SPI_CONTROLLER_READY) == 0) {}

    /*
     * Create synchronization semaphore; this semaphore will block the peripheral
     * until a transfer is complete.
     */
    /* Create semaphore instance */
    peripheralSem = Semaphore_create(0, &params, &eb);
    if(peripheralSem == NULL) {
#if DEBUG
        Display_printf(display, 0, 0, "Semaphore creation failed\n");
#endif
        while(1);
    }

    /* Create a semaphore for Async TX */
    /* Init params */
    /* Create semaphore instance */
    txDoneSem = Semaphore_create(0, &params, &eb);
    if(txDoneSem == NULL) {
#if DEBUG
        Display_printf(display, 0, 0, "txDone Semaphore creation failed\n");
#endif
    }

    /* Wait until master SPI is open.  */
    while (GPIO_read(CONFIG_SPI_CONTROLLER_READY)) {}
#if DEBUG
    Display_printf(display, 0, 0, "CONFIG_SPI_CONTROLLER_READY\n");
#endif

    /*
     * Open SPI as peripheral in callback mode; callback mode is used to allow us to
     * configure the transfer & then set CONFIG_SPI_RADIO_READY high.
     */
    SPI_Params_init(&spiParams);
    spiParams.frameFormat = SPI_POL0_PHA1;
    spiParams.mode = SPI_SLAVE;
    spiParams.transferCallbackFxn = transferCompleteFxn;
    spiParams.transferMode = SPI_MODE_CALLBACK;
    peripheralSpi = SPI_open(CONFIG_SPI_SLAVE, &spiParams);
    if (peripheralSpi == NULL) {
#if DEBUG
        Display_printf(display, 0, 0, "Error initializing peripheral SPI\n");
#endif
        while (1);
    } else {
#if DEBUG
        Display_printf(display, 0, 0, "Peripheral SPI initialized\n");
#endif
    }

    /* Copy message to transmit buffer */
    peripheralTxBuffer[0] = 0x5;
    peripheralTxBuffer[1] = 0x10;
    peripheralTxBuffer[2] = 0x15;
    peripheralTxBuffer[3] = 0x20;


    /* Initialize peripheral SPI transaction structure */
    memset((void *) peripheralRxBuffer, 0, SPI_MSG_LENGTH);
    transaction.count = SPI_MSG_LENGTH;
    transaction.txBuf = (void *) peripheralTxBuffer;
    transaction.rxBuf = (void *) peripheralRxBuffer;

    while(1) {
        EasyLink_TxPacket txPacket =  { {0}, 0, 0, {0} };

        txPacket.len = RFEASYLINKTXPAYLOAD_LENGTH;
        /*
         * Address filtering is enabled by default on the Rx device with the
         * an address of 0xAA. This device must set the dstAddr accordingly.
         */
        txPacket.dstAddr[0] = 0xaa;
        txPacket.absTime = 0;

#if DEBUG
        /* Toggle on user LED, indicating a SPI transfer is in progress */
        GPIO_toggle(CONFIG_GPIO_LED_1);
        Display_printf(display, 0, 0, "Transfer in progress\n");
#endif
        /*
         * Setup SPI transfer; CONFIG_SPI_RADIO_READY will be set to notify
         * master the peripheral is ready.
         */
        transferOK = SPI_transfer(peripheralSpi, &transaction);
        if (transferOK) {
            GPIO_write(CONFIG_SPI_RADIO_READY, 0);
#if DEBUG
            Display_printf(display, 0, 0, "Wait until transfer is complete\n");
#endif
            /* Wait until transfer has completed */
            Semaphore_pend(peripheralSem, BIOS_WAIT_FOREVER);

            /*
             * Drive CONFIG_SPI_RADIO_READY high to indicate peripheral is not ready
             * for another transfer yet.
             */
            GPIO_write(CONFIG_SPI_RADIO_READY, 1);

            if (transferStatus == false) {
#if DEBUG
                Display_printf(display, 0, 0, "SPI transfer failed!");
#endif
            } else {
#if DEBUG
                Display_printf(display, 0, 0, "peripheral received: %x", peripheralRxBuffer[0]);
#endif
                // Read payload to sent from the main controller
                for (i = 0; i < RFEASYLINKTXPAYLOAD_LENGTH; i++) {
                    txPacket.payload[i] = peripheralRxBuffer[i];
                }

                /* transmit payload */
                EasyLink_transmitAsync(&txPacket, txDoneCb);

                /* Wait for Tx to complete. */
                Semaphore_pend(txDoneSem, BIOS_WAIT_FOREVER);
                /* Switch to Receiver */
                EasyLink_receiveAsync(rxAckDoneCb, 0);
                /* Wait 10ms for Rx to complete */
                if(Semaphore_pend(txDoneSem, (txTimeOut / Clock_tickPeriod)) == FALSE) {
                    ackStatus = 0;
                    /* RX timed out, abort */
                    if(EasyLink_abort() == EasyLink_Status_Success) {
                       /* Abort will cause the rxAckDoneCb to be called */
                      Semaphore_pend(txDoneSem, BIOS_WAIT_FOREVER);
                    }
                } else {
                    ackStatus = 1;
                }

            }
        } else {
#if DEBUG
            Display_printf(display, 0, 0, "Unsuccessful peripheral SPI transfer");
#endif
        }

        peripheralTxBuffer[0] = ackStatus;
        // Send the ack status to the controller
        transferOK = SPI_transfer(peripheralSpi, &transaction);

        if (transferOK) {
            GPIO_write(CONFIG_SPI_RADIO_READY, 0);
#if DEBUG
            Display_printf(display, 0, 0, "Wait until transfer is complete\n");
#endif
            /* Wait until transfer has completed */
            Semaphore_pend(peripheralSem, BIOS_WAIT_FOREVER);

            /*
             * Drive CONFIG_SPI_RADIO_READY high to indicate peripheral is not ready
             * for another transfer yet.
             */
            GPIO_write(CONFIG_SPI_RADIO_READY, 1);

            if (transferStatus == false) {
#if DEBUG
                Display_printf(display, 0, 0, "SPI transfer failed!");
#endif
            } else {
#if DEBUG
                Display_printf(display, 0, 0, "peripheral received: %x", peripheralRxBuffer[0]);
#endif
            }
        }
    }
}

void radioTxTask_init() {
    Task_Params_init(&radioTxTaskParams);
    radioTxTaskParams.stackSize = TASKSTACKSIZE;
    radioTxTaskParams.stack = &radioTxTaskStack;
    Task_construct(&radioTxTask, (Task_FuncPtr)radioTxFxn, &radioTxTaskParams, NULL);
}

int main(void) {
    /* Call driver init functions. */
    Board_initGeneral();
    Display_init();
    GPIO_init();
    SPI_init();

#if DEBUG
    /* Configure the LED pins */
    GPIO_setConfig(CONFIG_GPIO_LED_0, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_GPIO_LED_1, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
#endif

    // Power monitor pin set to high
    GPIO_setConfig(CONFIG_GPIO_POWER_TX, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_HIGH);

    /* Open the display for output */
    display = Display_open(Display_Type_UART, NULL);
    if (display == NULL) {
        /* Failed to open display driver */
        while (1);
    }

    radioTxTask_init();

    /* Start BIOS */
    BIOS_start();

    return (NULL);
}
