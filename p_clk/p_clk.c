/*
 * Persistent clock application which provides time info to the main controller via I2C
 */
#include <msp430.h>
#include <stdio.h>
#include <stdint.h>

#define TX_DATA_LENGTH 2

volatile uint8_t ovfCount = 0;
volatile uint8_t timeStamp = 0;
uint8_t txBuffer[TX_DATA_LENGTH];
uint8_t buffIdx = 0;
char readstr[20];

void clockConfig()
{
    // Init clocks and I/O:
    // Startup clock system with DCO setting ~8MHz
    CSCTL0_H = CSKEY >> 8;                      // CSKey=A500.  Unlock clock registers
    CSCTL1 = DCOFSEL_6;                         // Set DCO to 8MHz.  6|40
    CSCTL2 = SELA__VLOCLK | SELS__DCOCLK | SELM__DCOCLK;
    CSCTL3 = DIVA__1 | DIVS__1 | DIVM__1;       // Set all dividers
    CSCTL0_H = 0;                               // Lock CS registers
}

void gpioConfig() {
    // Configure GPIO
    // EUCB1 for I2C
    P5SEL0 |= BIT0 | BIT1;
    P5SEL1 &= ~(BIT0 | BIT1);
    P1OUT &= ~BIT0;                         // Clear P1.0 output latch
    P1DIR |= BIT0;                          // For LED

    // Disable the GPIO power-on default high-impedance mode to activate
    // previously configured port settings
    PM5CTL0 &= ~LOCKLPM5;
}

void i2cConfig() {
    // Configure USCI_B1 for I2C mode
    UCB1CTLW0 = UCSWRST;                    // Software reset enabled
    UCB1CTLW0 |= UCMODE_3 | UCSYNC;         // I2C mode, sync mode
    UCB1I2COA0 = 0x48 | UCOAEN;             // own address is 0x48 + enable
    UCB1CTLW0 &= ~UCSWRST;                  // clear reset register
    UCB1IE |= UCTXIE0 | UCSTPIE;            // transmit,stop interrupt enable
}

void timerConfig() {
    // Timer0_A3 Setup
    /*
     * Capture both edge, Use CCI2B=ACLK, // Synchronous capture,
     * Enable capture mode, Enable capture interrupt
     * */
    TA0CCTL2 = CM__BOTH | CCIS_1 | SCS | CAP | CCIE;
    TA0CCTL2 ^= CCIS_0;
    TA0CTL = TASSEL__SMCLK | MC__CONTINUOUS | TACLR | TAIE;// Use SMCLK as clock source,
                                            // Start timer in continuous mode
}

void uartConfig() {
    // Set P2.0 and P2.1 for USCI_A0 UART operation
    P2SEL0 &= ~(BIT0 | BIT1);
    P2SEL1 |= BIT0 | BIT1;
    UCA0CTLW0 = UCSWRST;                      // Put eUSCI in reset
    UCA0CTLW0 |= UCSSEL__SMCLK;               // CLK = SMCLK
    UCA0BRW = 4;                             // 8000000/16/9600
    UCA0MCTLW |= UCOS16 | UCBRF_5 | 0x55;
    UCA0CTLW0 &= ~UCSWRST;
}

void uartTX(char * tx_data)                 // Define a function which accepts a character pointer to an array
{
    unsigned int i=0;
    while(tx_data[i])                        // Increment through array, look for null pointer (0) at end of string
    {
        while ((UCA0STATW & UCBUSY));        // Wait if line TX/RX module is busy with data
        UCA0TXBUF = tx_data[i];              // Send out element i of tx_data array on UART bus
        i++;                                 // Increment variable for array address
    }
}


int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;

    clockConfig();
    gpioConfig();
    i2cConfig();
    timerConfig();
    uartConfig();

    while (1){
        __bis_SR_register(GIE);         // Enable global interrupts

//        sprintf(readstr, "%x, ", timeStamp);
//        uartTX(readstr);
//        sprintf(readstr, "%x \r\n", ovfCount);
//        uartTX(readstr);

        // Update TX buffer
        txBuffer[0] = timeStamp;
        txBuffer[1] = ovfCount;
    }
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = EUSCI_B1_VECTOR
__interrupt void USCI_B1_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(EUSCI_B1_VECTOR))) USCI_B1_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch(__even_in_range(UCB1IV, USCI_I2C_UCBIT9IFG))
    {
        case USCI_NONE:          break;     // Vector 0: No interrupts
        case USCI_I2C_UCALIFG:   break;     // Vector 2: ALIFG
        case USCI_I2C_UCNACKIFG: break;     // Vector 4: NACKIFG
        case USCI_I2C_UCSTTIFG:  break;     // Vector 6: STTIFG
        case USCI_I2C_UCSTPIFG:             // Vector 8: STPIFG
            buffIdx = 0;
            ovfCount = 0;
            UCB1IFG &= ~UCSTPIFG;           // Clear stop condition int flag
            break;
        case USCI_I2C_UCRXIFG3:  break;     // Vector 10: RXIFG3
        case USCI_I2C_UCTXIFG3:  break;     // Vector 12: TXIFG3
        case USCI_I2C_UCRXIFG2:  break;     // Vector 14: RXIFG2
        case USCI_I2C_UCTXIFG2:  break;     // Vector 16: TXIFG2
        case USCI_I2C_UCRXIFG1:  break;     // Vector 18: RXIFG1
        case USCI_I2C_UCTXIFG1:  break;     // Vector 20: TXIFG1
        case USCI_I2C_UCRXIFG0:  break;     // Vector 22: RXIFG0
        case USCI_I2C_UCTXIFG0:             // Vector 24: TXIFG0
            UCB1TXBUF = txBuffer[buffIdx++];
            P1OUT ^= BIT0;
            break;
        case USCI_I2C_UCBCNTIFG: break;     // Vector 26: BCNTIFG
        case USCI_I2C_UCCLTOIFG: break;     // Vector 28: clock low timeout
        case USCI_I2C_UCBIT9IFG: break;     // Vector 30: 9th bit
        default: break;
    }
}

// Timer0_A3 CC1-4, TA Interrupt Handler
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = TIMER0_A1_VECTOR
__interrupt void Timer0_A1_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER0_A1_VECTOR))) Timer0_A1_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch (__even_in_range(TA0IV, TAIV__TAIFG))
    {
        case TAIV__TACCR1: break;
        case TAIV__TACCR2:
            // record timestamps when interrupts occur at both edges
            timeStamp = TA0CCR2;
            break;
        case TAIV__TAIFG:
            // Timer overflow counter
            ovfCount++;
            break;
        default: break;
    }
}

